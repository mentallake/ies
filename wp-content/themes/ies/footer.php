<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

// Get projects page id
$projects_page = get_page_by_path( 'projects' );
$projects_page_id = $projects_page->ID;
$projects_page_url = get_page_link($projects_page_id);

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;

$address = "";
reset_rows();

if( have_rows('address_group', $contact_page_id) ): the_row();
	$label_title = get_sub_field('address_label_text', $contact_page_id);
	$address = get_sub_field('address', $contact_page_id);
endif;

$full_company_name = get_field("full_company_name", $contact_page_id);
$short_company_name = get_field("short_company_name", $contact_page_id);

$footer_menus = get_field("footer_menus", $contact_page_id);
$subsidiary_company = get_field("subsidiary_company", $contact_page_id);
$social_list = get_field("social_list", $contact_page_id);
$factsheets = get_field("factsheets", $contact_page_id);
$view_all_factsheets_link_text = get_field("view_all_factsheets_link_text", $contact_page_id);

// Get all available project types
$args = array(
	'posts_per_page'   => '-1',
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	// 'orderby'          => 'date',
	// 'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	// 'meta_key'         => '',
	// 'meta_value'       => '',
	'post_type'        => 'project_type',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$all_project_types_query = get_posts($args);

$all_project_types = array();

foreach ($all_project_types_query as $post) : setup_postdata($post);
	$type_id = $post->ID;
	$type_title = get_field('project_type_title');
	$type_subtitle = get_field('project_type_subtitle');

	$type_info = array(
		"title" => $type_title,
		"subtitle" => $type_subtitle
	);

	$all_project_types[$type_id] = $type_info;
endforeach;
wp_reset_postdata();
?>
		<a id="go-to-top-btn"><i class="fa fa-angle-up"></i></a>

		<footer id="footer-panel">
	 		<div id="footer-inner-panel"
	 			 class="animate"
				 data-os-animation="fadeIn"
	 		 	 data-os-animation-delay="0.3s">
				<div id="footer-top-panel"></div>
				<div id="footer-body-panel">
					<div class="container">
						<div class="row narrow">
							<div class="col-sm-4">
								<div id="company-footer-panel">
									<div class="company-logo-footer">
										<a href="<?php echo home_url(); ?>">
											<img src="<?php echo get_template_directory_uri() . '/images/ies-logo.svg'; ?>" alt="">
										</a>
									</div>
									<div class="company-title-footer"><?php echo $full_company_name; ?></div>
									<div class="company-address-footer">
										<?php echo $address; ?>
									</div>
								</div>
							</div>
							<div class="col-sm-8">
								<div class="row narrow">
									<div class="footer-col col-xs-6 col-sm-3">
										<div class="footer-col-title">
											<?php the_field("footer_menu_section_title", $contact_page_id); ?>
										</div>
										<ul class="footer-menu">
											<?php
											foreach($footer_menus as $f_menu){
												$url = $f_menu["page"];
												$post_id = url_to_postid($url);

												$page_title = get_the_title($post_id);
											?>
											<li><a href="<?php echo $url; ?>"><?php echo $page_title; ?></a></li>
											<?php } ?>
										</ul>
									</div>
									<div class="footer-col col-xs-6 col-sm-3">
										<div class="footer-col-title">
											<?php the_field("project_section_title", $contact_page_id); ?>
										</div>
										<ul class="footer-menu">
											<?php
											foreach($all_project_types as $key => $value){
												$pt_id = $key;
												$pt_info = $value;
												$pt_title = $pt_info["title"];
												$pt_subtitle = $pt_info["subtitle"];
												$search_url = $projects_page_url . '?nproject_type=' . $pt_id;
											?>
											<li><a href="<?php echo $search_url; ?>"><?php echo $pt_title; ?></a></li>
											<?php } ?>
										</ul>
									</div>
									<div class="footer-col col-xs-6 col-sm-3">
										<div class="footer-col-title">
											<?php the_field("subsidiary_section_title", $contact_page_id); ?>
										</div>
										<ul class="footer-menu">
											<?php
											foreach($subsidiary_company as $sub_company){
												$company_name = $sub_company['company_name'];
												$company_website_url = $sub_company['company_website_url'];
											?>
											<li><a target="_blank" href="<?php echo $company_website_url; ?>"><?php echo $company_name; ?></a></li>
											<?php } ?>
										</ul>

										<div class="footer-col-title">
											<?php the_field("social_section_title", $contact_page_id); ?>
										</div>
										<ul class="footer-menu">
											<?php
											foreach($social_list as $social_item){
												$social_name = $social_item['social_name'];
												$social_url = $social_item['social_url'];
											?>
											<li><a target="_blank" href="<?php echo $social_url; ?>"><?php echo $social_name; ?></a></li>
											<?php } ?>
										</ul>
									</div>
									<div class="footer-col col-xs-6 col-sm-3">
										<div class="footer-col-title">
											<?php the_field("factsheet_section_title", $contact_page_id); ?>
										</div>
										<ul id="footer-factsheet-list" class="footer-menu factsheet-list">
											<?php
											$count = 0;
											$first_show_limit = 2;

											foreach($factsheets as $factsheet){
												$count++;
												$factsheet_title = $factsheet['factsheet_title'];
												$factsheet_files = $factsheet['factsheet_file'];

												$hide_class = $count > $first_show_limit ? "hide" : "";
											?>
											<li class="<?php echo $hide_class; ?>">
												<?php echo $factsheet_title; ?></a>

												<ul class="factsheet-files">
													<?php
													for($ii = 0; $ii < count($factsheet_files); $ii++){
														$language = $factsheet_files[$ii]['language'];
														$factsheet_url = $factsheet_files[$ii]['file'];
													?>
													<li>
														<a href="<?php echo $factsheet_url; ?>" target="_blank"><?php echo $language; ?></a>
													</li>
													<?php } ?>
												</ul>
											</li>
											<?php } ?>

											<?php if($count > $first_show_limit){ ?>
											<li class="show-all"><a id="show-all-factsheets"><?php echo $view_all_factsheets_link_text; ?> <i class="fa fa-angle-right"></i></a></li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="footer-bottom-panel">
					<div class="container">
						<div class="row">
							<div class="col-sm-7">
								© <?php echo date("Y"); ?> <?php echo $full_company_name; ?>. All Rights Reserved.
							</div>
							<div id="footer-bottom-right-col" class="col-sm-5">
								<a href="" class="hide">Terms & Privacy</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<div id="download-factsheet-dialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title title-text-6">
                        	All Factsheets
                        </h4>
                    </div>
                    <div class="modal-body">
                    	<?php
                    	$company_factsheet = array();
                    	$project_factsheet = array();

                    	foreach($factsheets as $factsheet){
                    		$factsheet_type = $factsheet['factsheet_type'];

							if($factsheet_type == 'company'){
								$company_factsheet[] = $factsheet;
							}else if($factsheet_type == 'project'){
								$project_factsheet[] = $factsheet;
							}
						}
                    	?>
                		<div class="row">
                			<div class="col-sm-6">
                				<div class="col-title">
                					Company
                				</div>
                				<ul id="" class="factsheet-list">
									<?php
									foreach($company_factsheet as $factsheet){
										$factsheet_title = $factsheet['factsheet_title'];
										$factsheet_files = $factsheet['factsheet_file'];
									?>
									<li>
										<?php echo $factsheet_title; ?></a>

										<ul class="factsheet-files">
											<?php
											for($ii = 0; $ii < count($factsheet_files); $ii++){
												$language = $factsheet_files[$ii]['language'];
												$factsheet_url = $factsheet_files[$ii]['file'];
											?>
											<li>
												<a href="<?php echo $factsheet_url; ?>" target="_blank"><?php echo $language; ?></a>
											</li>
											<?php } ?>
										</ul>
									</li>
									<?php } ?>
								</ul>
                			</div>
                			<div class="col-sm-6">
                				<div class="col-title">
                					Project
                				</div>
                				<ul id="" class="factsheet-list">
									<?php
									foreach($project_factsheet as $factsheet){
										$factsheet_title = $factsheet['factsheet_title'];
										$factsheet_files = $factsheet['factsheet_file'];
									?>
									<li>
										<?php echo $factsheet_title; ?></a>

										<ul class="factsheet-files">
											<?php
											for($ii = 0; $ii < count($factsheet_files); $ii++){
												$language = $factsheet_files[$ii]['language'];
												$factsheet_url = $factsheet_files[$ii]['file'];
											?>
											<li>
												<a href="<?php echo $factsheet_url; ?>" target="_blank"><?php echo $language; ?></a>
											</li>
											<?php } ?>
										</ul>
									</li>
									<?php } ?>
								</ul>
                			</div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>

		<div id="message-dialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title title-text-6">
                        	<img class="logo" src="<?php echo get_template_directory_uri() . "/images/ies-logo.svg"; ?>" alt="">
                        </h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer text-center-force">
                        <button type="button" id="ok-button" data-dismiss="modal" class="btn btn-red-1 size-2 btn-rounded big">Dismiss</button>
                    </div>
                </div>
            </div>
        </div>

		<?php wp_footer(); ?>
	</body>
</html>