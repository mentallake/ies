<?php
require_once("../../../wp-load.php");

// if the submit button is clicked, send the email
if ( isset( $_POST['contact_name'] ) ) {
    // sanitize form values
    $name = sanitize_text_field($_POST["contact_name"]);
    $company = sanitize_text_field($_POST["contact_company"]);
    $mobile = sanitize_text_field($_POST["contact_mobile"]);
    $email = sanitize_email($_POST["contact_email"]);
    $message = esc_textarea($_POST["contact_message"]);

    // Insert database
    global $wpdb;
    $table_name = $wpdb->prefix . "contact";

    $wpdb->insert($table_name, array(
                    'name' => $name,
                    'email' => $email,
                    'mobile' => $mobile,
                    'company' => $company,
                    'message' => $message,
                )
            );

    $thankyou_page = get_page_by_path( 'thank-you' );
    $thankyou_page_id = $thankyou_page->ID;
    $thankyou_page_url = get_page_link($thankyou_page_id);

    $contact_page_url = $_POST["contact_page_url"];

    $mail_subject = "[IES] " . $name . " has submitted contact form";

    // Html
    $mail_message = "Here is an auto mail from IES website. Someone has just submitted the contact form.<br><br>";
    $mail_message .= "<table><tbody>";
    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Name:</td>";
    $mail_message .= "<td>$name</td>";
    $mail_message .= "</tr>";

    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Company:</td>";
    $mail_message .= "<td>$company</td>";
    $mail_message .= "</tr>";

    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Mobile:</td>";
    $mail_message .= "<td>$mobile</td>";
    $mail_message .= "</tr>";

    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Email:</td>";
    $mail_message .= "<td>$email</td>";
    $mail_message .= "</tr>";

    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Message:</td>";
    $mail_message .= "<td>" . nl2br($message) . "</td>";
    $mail_message .= "</tr>";
    $mail_message .= "</tbody></table>";

    // get the blog administrator's email address
    $contact_page = get_page_by_path( 'contact' );
    $contact_page_id = $contact_page->ID;

    if( have_rows('email_group', $contact_page_id) ){
        the_row();
        $to_email = get_sub_field('email', $contact_page_id);
    }else{
        $to_email = "info@impactelectrons.com";
    }

    $to = $to_email;

    $headers = "MIME-Version: 1.0";
    $headers .= "Content-Type: text/html; charset=UTF-8";

    // If email has been process for sending, display a success message
    if ( wp_mail( $to, $mail_subject, $mail_message, $headers ) ) {
        $result = true;
        $message = "Thank you $name for contacting us, we will reach you soon.";
    } else {
        $result = false;
        $message = "Something wrong happened, we could not receive your message. You could call us directly.<br><br>Sorry for your inconvenience.";
    }

    $redirect_url = $thankyou_page_url . "?ncontent_type=contact";

    echo json_encode(array(
        "result" => $result,
        "message" => $message,
        "redirect_url" => $redirect_url
    ));
}else{
    $result = false;
    $message = "No any submission";

    echo json_encode(array(
        "result" => $result,
        "message" => $message
    ));
}
?>