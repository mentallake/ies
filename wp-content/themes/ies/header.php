<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage ies
 * @since IES 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width">

		<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri() . '/images/favicon.ico'; ?>">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<?php
		$lang_code = qtranxf_getLanguage();
		?>
		<input type="hidden" id="language-code" value="<?php echo $lang_code; ?>">

		<header id="header-panel" class="">
			<nav class="navbar navbar-fixed-top">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menus" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<!-- <span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span> -->
							<div class="menu menu-btn" data-menu="11">
						        <div class="icon-left"></div>
						        <div class="icon-right"></div>
						    </div>
						</button>
						<a class="navbar-brand" href="<?php echo home_url(); ?>">
						</a>
					</div>

					<?php
					wp_nav_menu( array(
		                'depth'             => 2,
		                'menu_class'        => 'nav navbar-nav navbar-right',
		                'container'         => 'div',
		                'container_class'   => 'collapse navbar-collapse',
		                'container_id'      => 'main-menus',
		                'walker'            => new wp_bootstrap_navwalker())
		            );
					?>
				</div><!-- /.container-fluid -->
			</nav>
		</header>

		<div id="page" class="hfeed site">
			<div id="sidebar" class="sidebar hide">
				<header id="masthead" class="site-header" role="banner">
					<div class="site-branding">
						<?php
							if ( is_front_page() && is_home() ) : ?>
								<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php else : ?>
								<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
							<?php endif;

							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
								<p class="site-description"><?php echo $description; ?></p>
							<?php endif;
						?>
						<button class="secondary-toggle"><?php _e( 'Menu and widgets', 'ies' ); ?></button>
					</div><!-- .site-branding -->
				</header><!-- .site-header -->

				<?php get_sidebar(); ?>
			</div><!-- .sidebar -->

			<div id="content" class="site-content">
