<?php
/**
 * The News template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;
$post_slug = $post->post_name;
$page_title = get_the_title();

$featured_news_text = get_field('featured_news_text');
$latest_news_text = get_field('latest_news_text');
$older_news_text = get_field('older_news_text');

$compare_operator = '=';
$meta_query = array();

// Get featured news
$args = array(
	'posts_per_page'   => '-1',
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	'orderby'          => 'meta_value',
	'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	'meta_key'         => 'date',
	// 'meta_value'       => '',
	'post_type'        => 'news',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$more_query = array('key' => 'is_featured',
					'value' => 1,
					'compare' => $compare_operator
					);

$meta_query[] = $more_query;

if(count($meta_query) > 0){
	$args['meta_query'] = $meta_query;
}

$featured_news = get_posts($args);

// Get normal news
$load_number = 6;

$count_posts = wp_count_posts('news')->publish;
$all_are_loaded = $count_posts - count($featured_news) <= $load_number;

$meta_query = array();

$args = array(
	'posts_per_page'   => $load_number,
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	'orderby'          => 'meta_value',
	'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	'meta_key'         => 'date',
	// 'meta_value'       => '',
	'post_type'        => 'news',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$more_query = array('key' => 'is_featured',
					'value' => 0,
					'compare' => $compare_operator
					);

$meta_query[] = $more_query;

if(count($meta_query) > 0){
	$args['meta_query'] = $meta_query;
}

$normal_news = get_posts($args);

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);

$start_project_with_us_text = get_field("start_project_with_us_text", $contact_page_id);
?>

<div id="news-page" class="content-page">
	<section id="feature-news"
			 class="content-section animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="section-title extra-small"><?php echo $featured_news_text; ?></div>
			<?php
			$is_more_than_one = count($featured_news) > 1;
			$row_class = $is_more_than_one ? "row" : "";
			$grid_class = $is_more_than_one ? "grid" : "";
			?>
			<ul class="news-list feature <?php echo $grid_class; ?> <?php echo $row_class; ?>">
			<?php if($is_more_than_one){ ?>
				<?php
				global $post;

				$time = 0.3;
				$f_count = 0;
				$f_limit = 2;

				foreach ($featured_news as $post) : setup_postdata($post);
					$f_count++;

					if($f_count > $f_limit){
						break;
					}

					$image = get_field('image');
					$title = get_field('news_title');
					$date = get_field('date');
					$short_description = get_field('short_description');
					$detail_url = get_permalink();
					$time += 0.15;
				?>
				<li class="col-sm-6 animate"
					data-os-animation="fadeIn"
	 		 		data-os-animation-delay="<?php echo $time; ?>s">
					<div class="news-item">
						<a href="<?php echo $detail_url; ?>">
							<div class="news-image" style="background-image: url(<?php echo $image; ?>);"></div>
							<div class="news-content">
								<div class="news-title"><?php echo $title; ?></div>
								<div class="news-date"><?php echo get_fulldate_display($date); ?></div>
							</div>
						</a>
					</div>
				</li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			<?php }else{ ?>
				<?php
				global $post;

				foreach ($featured_news as $post) : setup_postdata($post);
					$image = get_field('image');
					$title = get_field('news_title');
					$date = get_field('date');
					$short_description = get_field('short_description');
					$detail_url = get_permalink();
				?>
				<li class="news-item row no-gap">
					<div class="col-sm-6 animate"
						 data-os-animation="fadeIn"
	 		 			 data-os-animation-delay="0.3s">
						<div class="news-image" style="background-image: url(<?php echo $image; ?>);"></div>
					</div>
					<div class="col-sm-6">
						<div class="news-content">
							<div class="news-title"><?php echo $title; ?></div>
							<div class="news-date"><?php echo get_fulldate_display($date); ?></div>

							<div class="news-short-description">
								<p>
									<?php echo $short_description; ?>
								</p>

								<div class="button-panel">
									<a href="<?php echo $detail_url; ?>" class="btn link-btn text-red-1 more-detail">more detail</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			<?php } ?>
			</ul>
		</div>
	</section>

	<?php if(count($normal_news) > 0){ ?>
	<section id="latest-news"
			 class="content-section no-top-space animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="section-title extra-small"><?php echo $latest_news_text; ?></div>
			<ul class="news-list row grid">
				<?php
				global $post;

				$time = 0.3;

				foreach ($normal_news as $post) : setup_postdata($post);
					$n_id = $post->ID;
					$image = get_field('image');
					$title = get_field('news_title');
					$date = get_field('date');
					$detail_url = get_permalink();
					$time += 0.15;
				?>
				<li class="news-item col-sm-4 animate"
					data-id="<?php echo $n_id; ?>"
					data-os-animation="fadeIn"
	 		 		data-os-animation-delay="<?php echo $time; ?>s">
					<a href="<?php echo $detail_url; ?>">
						<div class="news-image" style="background-image: url(<?php echo $image; ?>);"></div>

						<div class="news-content">
							<div class="news-title"><?php echo $title; ?></div>
							<div class="news-date"><?php echo get_fulldate_display($date); ?></div>
						</div>
					</a>
				</li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</ul>
			<?php if(!$all_are_loaded){ ?>
			<div class="button-panel text-center">
	  			<a id="normal-news-load-more-btn" data-href="<?php echo get_template_directory_uri() . "/ajax_load_more.php"; ?>" class="load-more-btn btn btn-rounded btn-skelleton btn-red-1 big">
	  				<?php echo $older_news_text; ?>
	  			</a>
	  		</div>
	  		<?php } ?>
		</div>
	</section>
	<?php } ?>

	<section id="news-pre-footer"
			 class="pre-footer text-center animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="pre-footer-content">
				<div class="title-1 text-primary">
					<?php echo $start_project_with_us_text; ?>

					<a href="<?php echo $contact_page_url; ?>" class="icon text-red-1">
						<img src="<?php echo get_template_directory_uri() . '/images/right-arrow.svg'; ?>" alt="">
					</a>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();