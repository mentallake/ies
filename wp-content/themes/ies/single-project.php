<?php
/**
 * Template for single project page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
global $post;
$post_slug = $post->post_name;

$current_post_id = $post->ID;
$title = get_field('project_title');
$location = get_field('location');
$energy_production = get_field('energy_production');
$image = get_field('image');
$description = get_field('description');
$project_type = get_field('project_type');
$project_type_id = $project_type->ID;

// Get related projects
$args = array(
	'posts_per_page'   => '3',
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	// 'orderby'          => 'date',
	// 'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	'post__not_in' 	   => array( $current_post_id ),
	'meta_key'         => 'project_type',
	'meta_value'       => $project_type_id,
	'post_type'        => 'project',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$related_projects = get_posts($args);

// Get projects page id
$projects_page = get_page_by_path( 'projects' );
$projects_page_id = $projects_page->ID;
$projects_page_url = get_page_link($projects_page_id);

$back_to_all_projects_text = get_field('back_to_all_projects_text', $projects_page_id);

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);

$start_project_with_us_text = get_field("start_project_with_us_text", $contact_page_id);
?>

<div id="single-project-page" class="content-page">
	<section class="section-cover-image animate"
			 style="background-image: url(<?php echo $image; ?>);"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="mask"></div>
		<div class="section-content animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
			<div id="project-title" class="section-title"><?php echo $title; ?></div>
			<div id="project-subtitle" class="section-subtitle">
				<ul class="subtitle-text-list">
					<?php if($energy_production != ""){ ?>
					<li><?php echo $energy_production; ?></li>
					<?php } ?>
					<li><?php echo $location; ?></li>
				</ul>
			</div>
		</div>
	</section>
	<section class="post-content-panel animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.4s">
		<div class="container">
			<div class="post-content">
				<div class="post-body">
					<?php echo $description; ?>
				</div>

				<div class="post-footer">
					<a href="<?php echo $projects_page_url; ?>" class="btn link-btn text-red-1 text-normal">
						<?php echo $back_to_all_projects_text; ?>
					</a>
				</div>
			</div>
		</div>
	</section>

	<?php if(count($related_projects) > 0){ ?>
	<section id="related-projects"
			 class="animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container">
			<div class="related-project-content">
				<div class="section-title">Explore others projects</div>
				<ul class="project-list row">
					<?php
					foreach ($related_projects as $post) : setup_postdata($post);
						$rel_project_id = $post->ID;

						if($rel_project_id == $current_post_id){
							continue;
						}

						$project_title = get_field('project_title');
						$energy_production = get_field('energy_production');
						$location = get_field('location');
						$image = get_field('image');
						$project_type = get_field('project_type');
						$project_type_text = $project_type->project_type_title;
						$detail_url = get_permalink();
					?>
					<li class="col-sm-4">
						<a href="<?php echo $detail_url; ?>">
							<div class="project-image" style="background-image: url(<?php echo $image; ?>);"></div>
							<div class="project-content">
								<div class="project-type"><?php echo $project_type_text; ?></div>
								<div class="project-title"><?php echo $project_title; ?></div>
								<div class="project-subtitle">
									<ul class="subtitle-text-list">
										<li><?php echo $energy_production; ?></li>
										<li><?php echo $location; ?></li>
									</ul>
								</div>
							</div>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</section>
	<?php } ?>

	<section id="projects-pre-footer"
			 class="pre-footer text-center animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container">
			<div class="pre-footer-content">
				<div class="title-1 text-primary">
					<?php echo $start_project_with_us_text; ?>

					<a href="<?php echo $contact_page_url; ?>" class="icon text-red-1">
						<img src="<?php echo get_template_directory_uri() . '/images/right-arrow.svg'; ?>" alt="">
					</a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer();