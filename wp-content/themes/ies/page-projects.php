<?php
/**
 * The projects template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;
$post_slug = $post->post_name;
$page_title = get_field('page_title');
$introduction = get_field('introduction');
$section_title = get_field('what_we_have_done_title');
$description = get_field('what_we_have_done_description');
$section_title_2 = get_field('what_we_are_doing_title');
$description_2 = get_field('what_we_are_doing_description');
$current_url = get_permalink(get_page_by_path($post_slug));
$more_detail_text = get_field('more_detail_text');
$filter_by_text = get_field('filter_by_text');
$all_projects_text = get_field('all_projects_text');
$older_projects_text = get_field('older_projects_text');
$more_projects_text = get_field('more_projects_text');

$pj_type_query = isset($_GET['nproject_type']) ? $_GET['nproject_type'] : false;

$compare_operator = '=';
$meta_query = array();

$load_number = 3;

$args = array(
	'posts_per_page'   => $load_number,
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	'orderby'          => 'meta_value',
	'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	'meta_key'         => 'date',
	// 'meta_value'       => '',
	'post_type'        => 'project',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$meta_query[] = array('key' => 'is_project_finished',
				'value' => 1,
				'compare' => $compare_operator
				);

if($pj_type_query){
	$meta_query['relation'] = 'AND';
	$meta_query[] = array('key' => 'project_type',
						'value' => $pj_type_query,
						'compare' => $compare_operator
						);
}

$args['meta_query'] = $meta_query;

$done_projects = get_posts($args);

$args['posts_per_page'] = '-1';
$all_done_projects = count(get_posts($args));
$all_done_project_are_loaded = $all_done_projects <= $load_number;

// Get ongoing projects
$load_number = 4;
$meta_query = array();

$args = array(
	'posts_per_page'   => $load_number,
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	'orderby'          => 'meta_value',
	'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	'meta_key'         => 'date',
	// 'meta_value'       => '',
	'post_type'        => 'project',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$meta_query[] = array('key' => 'is_project_finished',
				'value' => 0,
				'compare' => $compare_operator
				);

if($pj_type_query){
	$meta_query['relation'] = 'AND';
	$meta_query[] = array('key' => 'project_type',
						'value' => $pj_type_query,
						'compare' => $compare_operator
						);
}

$args['meta_query'] = $meta_query;

$ongoing_projects = get_posts($args);

$args['posts_per_page'] = '-1';
$all_ongoing_projects = count(get_posts($args));
$all_ongoing_project_are_loaded = $all_ongoing_projects <= $load_number;

// Get all available project types
$args = array(
	'posts_per_page'   => '-1',
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	// 'orderby'          => 'date',
	// 'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	// 'meta_key'         => '',
	// 'meta_value'       => '',
	'post_type'        => 'project_type',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$all_project_types_query = get_posts($args);

$all_project_types = array(
	array(
		"title" => $all_projects_text,
		"subtitle" => ""
	)
);

foreach ($all_project_types_query as $post) : setup_postdata($post);
	$type_id = $post->ID;
	$type_title = get_field('project_type_title');
	$type_subtitle = get_field('project_type_subtitle');

	$type_info = array(
		"title" => $type_title,
		"subtitle" => $type_subtitle
	);

	$all_project_types[$type_id] = $type_info;
endforeach;
wp_reset_postdata();

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);

$start_project_with_us_text = get_field("start_project_with_us_text", $contact_page_id);
?>

<div id="projects-page" class="content-page">
	<section id="projects-intro"
			 class="content-section big-top-space animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<div class="page-title"><?php echo $page_title; ?></div>
					<div class="intro-description">
						<?php echo $introduction; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container animate hide"
		 data-os-animation="fadeIn"
	 	 data-os-animation-delay="0.3s">
		<hr>
	</div>

	<section id="project-filters-section"
			 class="animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.4s">
		<div id="project-filters-container" class="container nav-container">
			<div id="project-filters-label"><?php echo $filter_by_text; ?>:</div>

			<a class="nav-button prev"><i class="fa fa-angle-left"></i></a>
			<ul id="project-filters" class="owl-carousel1 owl-theme1">
				<?php
				foreach($all_project_types as $key => $value){
					$pt_id = $key;
					$pt_info = $value;
					$pt_title = $pt_info["title"];
					$pt_subtitle = $pt_info["subtitle"];
					$search_url = $pt_id == 0 ? $current_url : $current_url . '?nproject_type=' . $pt_id;

					$active_class = $pj_type_query == $pt_id ? 'active' : '';
				?>
				<li class="project-filter-item <?php echo $active_class; ?>">
					<a href="<?php echo $search_url; ?>" class="">
						<div class="content">
							<div class="title"><?php echo $pt_title; ?></div>
							<div class="subtitle"><?php echo $pt_subtitle; ?></div>
						</div>
					</a>
				</li>
				<?php } ?>
			</ul>
			<a class="nav-button next"><i class="fa fa-angle-right"></i></a>
		</div>
	</section>

	<?php if(count($done_projects) > 0){ ?>
	<section id="what-we-have-done"
			 class="animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="section-title">
				<?php echo $section_title; ?>
			</div>
			<div class="section-description">
				<?php echo $description; ?>
			</div>
			<ul class="project-list timeline">
				<?php
				global $post;

				foreach ($done_projects as $post) : setup_postdata($post);
					$p_id = $post->ID;
					$image = get_field('image');
					$title = get_field('project_title');
					$date = get_field('date');
					$location = get_field('location');
					$project_type = get_field('project_type');
					$proejct_type_text = $project_type->project_type_title;
					$short_description = get_field('short_description');
					$energy_production = get_field('energy_production');
					$project_type = get_field('project_type');
					$detail_url = get_permalink();
				?>
				<li class="project-item animate"
					data-id="<?php echo $p_id; ?>"
					data-os-animation="fadeIn"
	 		 		data-os-animation-delay="0.4s">
					<div class="project-time"><?php echo get_date_display($date); ?></div>

					<a href="<?php echo $detail_url; ?>">
						<div class="row no-gap">
							<div class="col-sm-5">
								<div class="project-image" style="background-image: url(<?php echo $image; ?>);"></div>
							</div>
							<div class="col-sm-7">
								<div class="project-content animate"
									 data-os-animation="fadeIn"
	 		 						 data-os-animation-delay="0.45s">
									<div class="project-type"><?php echo $proejct_type_text; ?></div>
									<div class="project-title"><?php echo $title; ?></div>
									<div class="project-subtitle">
										<ul class="subtitle-text-list">
											<?php if($energy_production != ""){ ?>
											<li><?php echo $energy_production; ?></li>
											<?php } ?>
											<li><?php echo $location; ?></li>
										</ul>
									</div>
									<div class="project-short-description hidden-xs">
										<div class="project-short-description-inner">
											<?php echo $short_description; ?>
										</div>
									</div>
									<div class="button-panel">
										<div class="more-detail text-red-1"><?php echo $more_detail_text; ?></div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</ul>

			<?php if(!$all_done_project_are_loaded){ ?>
			<div class="button-panel text-center more-project">
	  			<a id="done-project-load-more-btn" data-href="<?php echo get_template_directory_uri() . "/ajax_load_more.php"; ?>" class="btn btn-rounded btn-skelleton btn-red-1 big">
	  				<?php echo $older_projects_text; ?>
	  			</a>
	  		</div>
	  		<?php } ?>
		</div>
	</section>
	<?php } ?>

	<?php if(count($ongoing_projects) > 0){ ?>
	<section id="what-we-are-doing"
			 class="animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="section-title">
				<?php echo $section_title_2; ?>
			</div>
			<div class="section-description">
				<?php echo $description_2; ?>
			</div>
			<ul class="project-list row narrow">
				<?php
				global $post;

				foreach ($ongoing_projects as $post) : setup_postdata($post);
					$p_id = $post->ID;
					$image = get_field('image');
					$title = get_field('project_title');
					$date = get_field('date');
					$location = get_field('location');
					$project_type = get_field('project_type');
					$project_type_text = $project_type->project_type_title;
					$short_description = get_field('short_description');
					$energy_production = get_field('energy_production');
					$detail_url = get_permalink();
				?>
				<li class="project-item col-xs-6 col-md-4 animate"
					data-id="<?php echo $p_id; ?>"
					data-os-animation="fadeIn"
	 		 		data-os-animation-delay="0.4s">
					<a href="<?php echo $detail_url; ?>">
						<div class="project-image" style="background-image: url(<?php echo $image; ?>);"></div>
						<div class="project-content animate"
							 data-os-animation="fadeIn"
	 		 				 data-os-animation-delay="0.45s">
	 		 				<div class="project-type"><?php echo $project_type_text; ?></div>
							<div class="project-title"><?php echo $title; ?></div>
							<div class="project-subtitle">
								<ul class="subtitle-text-list">
									<?php if($energy_production != ""){ ?>
									<li><?php echo $energy_production; ?></li>
									<?php } ?>
									<li><?php echo $location; ?></li>
								</ul>
							</div>
						</div>
					</a>
				</li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</ul>

			<?php if(!$all_ongoing_project_are_loaded){ ?>
			<div class="button-panel more-project text-center">
	  			<a id="ongoing-project-load-more-btn" data-href="<?php echo get_template_directory_uri() . "/ajax_load_more.php"; ?>" class="btn btn-rounded btn-skelleton btn-red-1 big">
	  				<?php echo $more_projects_text; ?>
	  			</a>
	  		</div>
	  		<?php } ?>
		</div>
	</section>
	<?php } ?>

	<section id="projects-pre-footer"
			 class="pre-footer text-center animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="pre-footer-content">
				<div class="title-1 text-primary">
					<?php echo $start_project_with_us_text; ?>

					<a href="<?php echo $contact_page_url; ?>" class="icon text-red-1">
						<img src="<?php echo get_template_directory_uri() . '/images/right-arrow.svg'; ?>" alt="">
					</a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer();