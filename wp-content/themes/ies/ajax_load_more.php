<?php

require_once("../../../wp-load.php");

$load_number = 9;

if ( isset( $_POST['content_type'] ) ) {
	$content_type = $_POST['content_type'];
	$loaded_ids = isset($_POST['loaded_ids']) ? $_POST['loaded_ids'] : array();

	$compare_operator = '=';
	$meta_query = array();

	if($content_type == 'news'){
		$args = array(
			'posts_per_page'   => $load_number + 1,
			// 'offset'           => $offset,
			// 'category'         => $current_category_id,
			// 'category_name'    => '',
			'orderby'          => 'meta_value',
			'order'            => 'DESC',
			// 'include'          => '',
			'exclude'          => '',
			'meta_key'         => 'date',
			// 'meta_value'       => '',
			'post_type'        => 'news',
			// 'post_mime_type'   => '',
			// 'post_parent'      => '',
			// 'author'	   		  => '',
			// 'author_name'	  => '',
			// 'post_status'      => 'publish',
			// 'suppress_filters' => true,
			// 'tag' => $year,
			'post__not_in'	   => $loaded_ids
		);

		$more_query = array('key' => 'is_featured',
							'value' => 0,
							'compare' => $compare_operator
							);

		$meta_query[] = $more_query;

		if(count($meta_query) > 0){
			$args['meta_query'] = $meta_query;
		}

		$loaded_posts = get_posts($args);

		// Still have more in database
		$all_are_loaded = false;

		if(count($loaded_posts) <= $load_number){
			$all_are_loaded = true;
		}else{
			$loaded_posts = array_slice($loaded_posts, 0, count($loaded_posts) - 1);
		}

		$html = build_news_html($loaded_posts);

		$result = true;
    	$message = "";

		echo json_encode(array(
	        "result" => $result,
	        "message" => $message,
	        "data" => $html,
	        "all_are_loaded" => $all_are_loaded
	        // "redirect_url" => $redirect_url
	    ));

	    return;
	}else if($content_type == 'done-project'){
		$args = array(
			'posts_per_page'   => $load_number + 1,
			// 'offset'           => $offset,
			// 'category'         => $current_category_id,
			// 'category_name'    => '',
			'orderby'          => 'meta_value',
			'order'            => 'DESC',
			// 'include'          => '',
			'exclude'          => '',
			'meta_key'         => 'date',
			// 'meta_value'       => '',
			'post_type'        => 'project',
			// 'post_mime_type'   => '',
			// 'post_parent'      => '',
			// 'author'	   		  => '',
			// 'author_name'	  => '',
			// 'post_status'      => 'publish',
			// 'suppress_filters' => true,
			// 'tag' => $year,
			'post__not_in'	   => $loaded_ids
		);

		$more_query = array('key' => 'is_project_finished',
							'value' => 1,
							'compare' => $compare_operator
							);

		$meta_query[] = $more_query;

		if(count($meta_query) > 0){
			$args['meta_query'] = $meta_query;
		}

		$loaded_posts = get_posts($args);

		// Still have more in database
		$all_are_loaded = false;

		if(count($loaded_posts) <= $load_number){
			$all_are_loaded = true;
		}else{
			$loaded_posts = array_slice($loaded_posts, 0, count($loaded_posts) - 1);
		}

		$html = build_timeline_project_html($loaded_posts);

		$result = true;
    	$message = "";

		echo json_encode(array(
	        "result" => $result,
	        "message" => $message,
	        "data" => $html,
	        "all_are_loaded" => $all_are_loaded
	        // "redirect_url" => $redirect_url
	    ));

	    return;
	}else if($content_type == 'ongoing-project'){
		$args = array(
			'posts_per_page'   => $load_number + 1,
			// 'offset'           => $offset,
			// 'category'         => $current_category_id,
			// 'category_name'    => '',
			'orderby'          => 'meta_value',
			'order'            => 'DESC',
			// 'include'          => '',
			'exclude'          => '',
			'meta_key'         => 'date',
			// 'meta_value'       => '',
			'post_type'        => 'project',
			// 'post_mime_type'   => '',
			// 'post_parent'      => '',
			// 'author'	   		  => '',
			// 'author_name'	  => '',
			// 'post_status'      => 'publish',
			// 'suppress_filters' => true,
			// 'tag' => $year,
			'post__not_in'	   => $loaded_ids
		);

		$more_query = array('key' => 'is_project_finished',
							'value' => 0,
							'compare' => $compare_operator
							);

		$meta_query[] = $more_query;

		if(count($meta_query) > 0){
			$args['meta_query'] = $meta_query;
		}

		$loaded_posts = get_posts($args);

		// Still have more in database
		$all_are_loaded = false;

		if(count($loaded_posts) <= $load_number){
			$all_are_loaded = true;
		}else{
			$loaded_posts = array_slice($loaded_posts, 0, count($loaded_posts) - 1);
		}

		$html = build_grid_project_html($loaded_posts);

		$result = true;
    	$message = "";

		echo json_encode(array(
	        "result" => $result,
	        "message" => $message,
	        "data" => $html,
	        "all_are_loaded" => $all_are_loaded
	        // "redirect_url" => $redirect_url
	    ));

	    return;
	}

	$result = false;
    $message = "No handle for content type [" . $content_type . "]";

    echo json_encode(array(
        "result" => $result,
        "message" => $message
    ));
}else{
    $result = false;
    $message = "No any submission";

    echo json_encode(array(
        "result" => $result,
        "message" => $message
    ));
}

function build_news_html($news_list){
	$html = "";

	global $post;

	$time = 0;

	foreach ($news_list as $post) : setup_postdata($post);
		$n_id = $post->ID;
		$image = get_field('image');
		$title = get_field('news_title');
		$date = get_field('date');
		$detail_url = get_permalink();
		$time += 0.15;

		$html .=   '<li class="new news-item col-sm-4 animate"
						data-id="' . $n_id . '"
						data-os-animation="fadeIn"
				 		data-os-animation-delay="' . $time . 's">
						<a href="' . $detail_url . '">
							<div class="news-image" style="background-image: url(' . $image . ');"></div>

							<div class="news-content">
								<div class="news-title">' . $title . '</div>
								<div class="news-date">' . get_fulldate_display($date) . '</div>
							</div>
						</a>
					</li>';
	endforeach;
	wp_reset_postdata();

	return $html;
}

function build_timeline_project_html($project_list){
	$html = "";

	global $post;

	foreach ($project_list as $post) : setup_postdata($post);
		$p_id = $post->ID;
		$image = get_field('image');
		$title = get_field('project_title');
		$date = get_field('date');
		$location = get_field('location');
		$project_type = get_field('project_type');
		$project_type_text = isset($project_type->project_type_title) ? $project_type->project_type_title : "";
		$short_description = get_field('short_description');
		$energy_production = get_field('energy_production');
		$project_type = get_field('project_type');
		$detail_url = get_permalink();

		$html .=   '<li class="project-item animate"
						data-id="' . $p_id . '"
						data-os-animation="fadeIn"
					 		data-os-animation-delay="0.4s">
						<div class="project-time">' . get_date_display($date) . '</div>

						<a href="' . $detail_url . '">
							<div class="row no-gap">
								<div class="col-sm-5">
									<div class="project-image" style="background-image: url(' . $image . ');"></div>
								</div>
								<div class="col-sm-7">
									<div class="project-content animate"
										 data-os-animation="fadeIn"
					 						 data-os-animation-delay="0.45s">
										<div class="project-type">' . $project_type_text . '</div>
										<div class="project-title">' . $title . '</div>
										<div class="project-subtitle">
											<ul class="subtitle-text-list">
												<?php if($energy_production != ""){ ?>
												<li>' . $energy_production . '</li>
												<?php } ?>
												<li>' . $location . '</li>
											</ul>
										</div>
										<div class="project-short-description hidden-xs">
											<div class="project-short-description-inner">
												' . $short_description . '
											</div>
										</div>
										<div class="button-panel">
											<div class="more-detail text-red-1">more detail</div>
										</div>
									</div>
								</div>
							</div>
						</a>
					</li>';
	endforeach;
	wp_reset_postdata();

	return $html;
}

function build_grid_project_html($project_list){
	$html = "";

	global $post;

	foreach ($project_list as $post) : setup_postdata($post);
		$p_id = $post->ID;
		$image = get_field('image');
		$title = get_field('project_title');
		$date = get_field('date');
		$location = get_field('location');
		$project_type = get_field('project_type');
		$project_type_text = isset($project_type->project_type_title) ? $project_type->project_type_title : "";
		$short_description = get_field('short_description');
		$energy_production = get_field('energy_production');
		$project_type = get_field('project_type');
		$detail_url = get_permalink();

		$html .=   '<li class="project-item new col-xs-6 col-md-4 animate"
						data-id="' . $p_id . '"
						data-os-animation="fadeIn"
		 		 		data-os-animation-delay="0.4s">
						<a href="' . $detail_url . '">
							<div class="project-image" style="background-image: url(' . $image . ');"></div>
							<div class="project-content animate"
								 data-os-animation="fadeIn"
		 		 				 data-os-animation-delay="0.45s">
		 		 				<div class="project-type">' . $project_type_text . '</div>
								<div class="project-title">' . $title . '</div>
								<div class="project-subtitle">
									<ul class="subtitle-text-list">
										<?php if($energy_production != ""){ ?>
										<li>' . $energy_production . '</li>
										<?php } ?>
										<li>' . $location . '</li>
									</ul>
								</div>
							</div>
						</a>
					</li>';
	endforeach;
	wp_reset_postdata();

	return $html;
}
?>