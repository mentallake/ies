<?php
require_once('libs/wp_bootstrap_navwalker.php');

function add_scripts(){
	// Css
	wp_enqueue_style('reset-css', get_template_directory_uri() . '/css/reset.min.css');
	wp_enqueue_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css');
	wp_enqueue_style('font-awesome-css', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('kanit-font', 'https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500');
    wp_enqueue_style('prompt-font', 'https://fonts.googleapis.com/css?family=Prompt:300,300i,400,400i,500');
	wp_enqueue_style('bootstrap-css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css');
    wp_enqueue_style('owl-carousel-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css');
    wp_enqueue_style('owl-carousel-theme-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css');
    // wp_enqueue_style('ies-style', get_stylesheet_uri());
    $langcode = qtranxf_getLanguage();
    wp_enqueue_style( 'lang-style', get_template_directory_uri() . '/style-' . $langcode . '.css' );

	// wp_enqueue_style('slick-carousel-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css');
	// wp_enqueue_style('slick-carousel-theme-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.min.css');

	// Libs
	wp_enqueue_script('modernizr-js', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js');
	wp_enqueue_script('jquery-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js');
	wp_enqueue_script('jquery-mobile-js', get_template_directory_uri() . '/libs/jquery-mobile/jquery.mobile-1.4.5.custom.min.js');
	wp_enqueue_script('jquery-ui-js', 'https://code.jquery.com/ui/1.11.3/jquery-ui.js');
	wp_enqueue_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js');
    // wp_enqueue_script('bootstrap-select-js', get_template_directory_uri() . '/libs/bootstrap-select-1.12.2/js/bootstrap-select.min.js');
    wp_enqueue_script('owl-carousel-js', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js');
    // wp_enqueue_script('slick-carousel-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js');
    wp_enqueue_script('jquery-form-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js');
    wp_enqueue_script('waypoint-js', 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js');
    wp_enqueue_script('animate-number-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-animateNumber/0.0.14/jquery.animateNumber.min.js');
    wp_enqueue_script('truncate-js', get_template_directory_uri() . '/libs/truncate/truncate.min.js');
    wp_enqueue_script('matchHeight-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js');

	// Scripts
	wp_enqueue_script('app-js', get_template_directory_uri() . '/scripts/app.js');

	if(is_page('home')){
		wp_enqueue_script('home-js', get_template_directory_uri() . '/scripts/home.js');
	}else if(is_page('about')){
		wp_enqueue_script('about-js', get_template_directory_uri() . '/scripts/about.js');
	}else if(is_page('careers')){
		wp_enqueue_script('careers-js', get_template_directory_uri() . '/scripts/careers.js');
	}else if(is_page('news')){
		wp_enqueue_script('news-js', get_template_directory_uri() . '/scripts/news.js');
	}else if(is_page('projects')){
		wp_enqueue_script('projects-js', get_template_directory_uri() . '/scripts/projects.js');
	}else if(is_page('application')){
		wp_enqueue_script('application-js', get_template_directory_uri() . '/scripts/application.js');
	}else if(is_page('contact')){
		wp_enqueue_script('contact-js', get_template_directory_uri() . '/scripts/contact.js');
	}else if(is_tag()){
		wp_enqueue_script('tag-js', get_template_directory_uri() . '/scripts/tag.js');
	}
}

function add_local_scripts(){
	wp_enqueue_style('reset-css', get_template_directory_uri() . '/css/reset.min.css');
	wp_enqueue_style('animate-css', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/fonts/font-awesome-4.7.0/css/font-awesome.min.css');
	// wp_enqueue_style('kanit-font', 'https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500');
	wp_enqueue_style('prompt-font', get_template_directory_uri() . '/fonts/prompt/css/fonts.css');
	wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/libs/bootstrap-3.3.7/css/bootstrap.min.css');
	wp_enqueue_style('owl-carousel-css', get_template_directory_uri() . '/libs/OwlCarousel2-2.2.1/assets/owl.carousel.min.css');
	wp_enqueue_style('owl-carousel-theme-css', get_template_directory_uri() . '/libs/OwlCarousel2-2.2.1/assets/owl.theme.default.min.css');
	// wp_enqueue_style('jquery-ui-css', get_template_directory_uri() . '/libs/jquery-ui/jquery-ui.min.css');
	// wp_enqueue_style('jquery-mobile-css', get_template_directory_uri() . '/libs/jquery-mobile/jquery.mobile-1.4.5.min.css');
	// wp_enqueue_style('ies-style', get_stylesheet_uri());

    $langcode = qtranxf_getLanguage();
    wp_enqueue_style( 'lang-style', get_template_directory_uri() . '/style-' . $langcode . '.css' );


	// Libs
	wp_enqueue_script('modernizr-js', get_template_directory_uri() . '/libs/modernizr-2.8.3.min.js');
	wp_enqueue_script('jquery-js', get_template_directory_uri() . '/libs/jquery-1.12.4.min.js');
	wp_enqueue_script('jquery-mobile-js', get_template_directory_uri() . '/libs/jquery-mobile/jquery.mobile-1.4.5.custom.min.js');
	wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/libs/bootstrap-3.3.7/js/bootstrap.min.js');
	wp_enqueue_script('owl-carousel-js', get_template_directory_uri() . '/libs/OwlCarousel2-2.2.1/owl.carousel.min.js');
	wp_enqueue_script('jquery-form-js', get_template_directory_uri() . '/libs/jquery.form.min.js');
	wp_enqueue_script('waypoint-js', get_template_directory_uri() . '/libs/jquery.waypoint.min.js');
	wp_enqueue_script('animate-number-js', get_template_directory_uri() . '/libs/jquery.animateNumber.min.js');
	wp_enqueue_script('truncate-js', get_template_directory_uri() . '/libs/truncate/truncate.min.js');
	wp_enqueue_script('matchHeight-js', get_template_directory_uri() . '/libs/matchHeight/jquery.matchHeight-min.js');

	// Scripts
	wp_enqueue_script('app-js', get_template_directory_uri() . '/scripts/app.js');

	if(is_page('home')){
		wp_enqueue_script('home-js', get_template_directory_uri() . '/scripts/home.js');
	}else if(is_page('about')){
		wp_enqueue_script('about-js', get_template_directory_uri() . '/scripts/about.js');
	}else if(is_page('careers')){
		wp_enqueue_script('careers-js', get_template_directory_uri() . '/scripts/careers.js');
	}else if(is_page('news')){
		wp_enqueue_script('news-js', get_template_directory_uri() . '/scripts/news.js');
	}else if(is_page('projects')){
		wp_enqueue_script('projects-js', get_template_directory_uri() . '/scripts/projects.js');
	}else if(is_page('application')){
		wp_enqueue_script('application-js', get_template_directory_uri() . '/scripts/application.js');
	}else if(is_page('contact')){
		wp_enqueue_script('contact-js', get_template_directory_uri() . '/scripts/contact.js');
	}else if(is_tag()){
		wp_enqueue_script('tag-js', get_template_directory_uri() . '/scripts/tag.js');
	}
}

add_action( 'wp_enqueue_scripts', 'add_scripts' );
// add_action( 'wp_enqueue_scripts', 'add_local_scripts' );

// Add menus to WP Admin sidebar
add_theme_support( 'menus' );

// Hide admin bar at front end.
show_admin_bar( false );

// Update staff title
function update_staff_page_title( $value, $post_id, $field  ) {
	global $_POST;

	$new_title = get_field('staff_firstname', $post_id) . ' ' . $value;
	$new_slug = sanitize_title( $new_title );

	// update post
	// http://codex.wordpress.org/Function_Reference/wp_update_post
	$my_post = array(
      	'ID'           => $post_id,
      	'post_title' => $new_title,
	  	'post_name' => $new_slug
  	);

	// Update the post into the database
  	wp_update_post( $my_post );

  	return $value;
}

// acf/update_value - filter for every field
add_filter('acf/update_value/name=staff_lastname', 'update_staff_page_title', 10, 3);

// Update Project type title
function update_project_type_page_title( $value, $post_id, $field  ) {
	global $_POST;

	$new_title = $value;
	$new_slug = sanitize_title( $new_title );

	// update post
	// http://codex.wordpress.org/Function_Reference/wp_update_post
	$my_post = array(
      	'ID'           => $post_id,
      	'post_title' => $new_title,
	  	'post_name' => $new_slug
  	);

	// Update the post into the database
  	wp_update_post( $my_post );

  	return $value;
}

// acf/update_value - filter for every field
add_filter('acf/update_value/name=project_type_title', 'update_project_type_page_title', 10, 3);

// Update Project title
function update_project_title( $value, $post_id, $field  ) {
	global $_POST;

	// $project_type = get_field('project_type', $post_id);
	// $project_type_id = $project_type->ID;
	// $project_type_title = get_field('project_type_title', $project_type_id);

	// $new_title = '[' . $project_type_title . '] ' . $value;
	$new_title = get_field('project_title', $post_id);
	$new_slug = sanitize_title( $new_title );

	// update post
	// http://codex.wordpress.org/Function_Reference/wp_update_post
	$my_post = array(
      	'ID'           => $post_id,
      	'post_title' => $new_title,
	  	'post_name' => $new_slug
  	);

	// Update the post into the database
  	wp_update_post( $my_post );

  	return $value;
}

// acf/update_value - filter for every field
add_filter('acf/update_value/name=is_project_finished', 'update_project_title', 10, 3);

// Update News title
function update_news_title( $value, $post_id, $field  ) {
	global $_POST;

	// $project_type = get_field('project_type', $post_id);
	// $project_type_id = $project_type->ID;
	// $project_type_title = get_field('project_type_title', $project_type_id);

	// $new_title = '[' . $project_type_title . '] ' . $value;
	$new_title = get_field('news_title', $post_id);
	$new_slug = sanitize_title( $new_title );

	// update post
	// http://codex.wordpress.org/Function_Reference/wp_update_post
	$my_post = array(
      	'ID'           => $post_id,
      	'post_title' => $new_title,
	  	'post_name' => $new_slug
  	);

	// Update the post into the database
  	wp_update_post( $my_post );

  	return $value;
}

// acf/update_value - filter for every field
add_filter('acf/update_value/name=is_featured', 'update_news_title', 10, 3);


function get_thai_month($index) {
	$thai_month_arr=array(
		"",
		"มกราคม",
		"กุมภาพันธ์",
		"มีนาคม",
		"เมษายน",
		"พฤษภาคม",
		"มิถุนายน",
		"กรกฎาคม",
		"สิงหาคม",
		"กันยายน",
		"ตุลาคม",
		"พฤศจิกายน",
		"ธันวาคม"
	);

	return $thai_month_arr[$index];
}

function get_thai_date_display($date) {
	$datetime = DateTime::createFromFormat("d/m/Y", $date);
	$str = get_thai_month($datetime->format("n")) . " ";
	$str .= $datetime->format("Y");

	return $str;
}

function get_date_display($date) {
	$datetime = DateTime::createFromFormat("d/m/Y", $date);

	$current_lang = qtrans_getLanguage();
	setlocale(LC_ALL, 'en_EN');

	switch($current_lang){
		case 'th':
			return get_thai_date_display($date);
	}

	return $datetime->format("F Y");
	// $str = date("j ", $time);
	// $str .= date("F", $time) . " ";
	// $str .= date("Y", $time);

	// return $str;
}

function get_thai_fulldate_display($date) {
	$datetime = DateTime::createFromFormat("d/m/Y", $date);
	$str = $datetime->format("j") . " " ;
	$str .= get_thai_month($datetime->format("n")) . " ";
	$str .= $datetime->format("Y");

	return $str;
}

function get_fulldate_display($date) {
	$datetime = DateTime::createFromFormat("d/m/Y", $date);

	$current_lang = qtrans_getLanguage();
	setlocale(LC_ALL, 'en_EN');

	switch($current_lang){
		case 'th':
			return get_thai_fulldate_display($date);
	}

	return $datetime->format("j F Y");
}

// Set title
// function set_title($data){
//     global $post;

//     if(isset($post->post_title)){
//     	return 'IES | ' . $post->post_title;
//     }

//     return 'IES';
// }

// add_filter('wp_title','set_title');

// Enhance the searching to be able to search post with string and meta_query at the same time.
add_action( 'pre_get_posts', function( $q ){
	if( $title = $q->get( '_meta_or_title' ) ){
        add_filter( 'get_meta_sql', function( $sql ) use ( $title ){
            global $wpdb;

            // Only run once:
            static $nr = 0;
            if( 0 != $nr++ ) return $sql;

            // Modified WHERE
            $sql['where'] = sprintf(
                " AND ( %s OR %s ) ",
                $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title),
                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
            );

            return $sql;
        });
    }
});

add_action( 'pre_get_posts', function( $q ){
    if( $title = $q->get( '_meta_with_title' ) ){
        add_filter( 'get_meta_sql', function( $sql ) use ( $title ){
            global $wpdb;

            // Only run once:
            static $nr = 0;
            if( 0 != $nr++ ) return $sql;

            $search_text = "mt1.meta_key = 'post_title' AND mt1.meta_value LIKE '%" . $title . "%'";
            $sql['where'] = str_replace($search_text, $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title), $sql['where']);

            return $sql;
        });
    }
});

function init_google_map() {
	acf_update_setting('google_api_key', 'AIzaSyDXgVsSC_A7j9d0DCnT2FKOdPXZpJP5jiM');
}

add_action('acf/init', 'init_google_map');

function cvf_mail_content_type() {
    return "text/html";
}

add_filter ("wp_mail_content_type", "cvf_mail_content_type");

function display_post_title($data){
    global $post;

	if(is_tag()){
		$post_title = single_tag_title("IES | News Tag: ");
	}else{
		$post_title = 'IES | ' . $post->post_title;
    	$post_type = $post->post_type;

    	if($post_type == 'project'){
	    	$post_id = $post->ID;
	    	$post_title = 'IES | ' . get_field('project_title', $post_id);
	    }else if($post_type == 'news'){
	    	$post_id = $post->ID;
	    	$post_title = "IES | " . get_field('news_title', $post_id);
	    }

	    if(is_page('application')){
	    	$position_query = isset($_GET['position']) ? $_GET['position'] : false;
			$location_query = isset($_GET['location']) ? $_GET['location'] : false;

			if($position_query){
				$post_title = 'IES | ' . $position_query;
			}

			if($location_query){
				$post_title = $post_title . " (" . $location_query . ")";
			}
	    }
	}

    return $post_title;
}

add_filter('wp_title','display_post_title');

//Lets add Open Graph Meta Info
function insert_fb_in_head() {
    global $post;

    // If it is not a post or a page
    if ( !is_singular())
        return;

    $title = get_the_title();
    $url = get_permalink();

    $default_image="ies-logo-square.jpg";
    $image_url = get_template_directory_uri() . '/images/' . $default_image;

    // Get description
    $description = "We are passionate about our work, our people, and our brand. With passion come great achievements. It is what ultimately drives us to initiate “first mover” ideas and develop new ways to provide affordable clean energy for all.";

    // Page
    if(is_page()){
    	$post_id = $post->ID;

	    if(is_page('home')){
	    	$image_url = get_field('actual_image', $post_id)['url'];
	    }else if(is_page('about')){
	    	$description = get_field('introduction_description', $post_id);
	    	$image_url = get_field('introduction_image', $post_id);
	    }else if(is_page('projects')){
	    	$description = get_field('introduction', $post_id);
	    }else if(is_page('news')){
	    }else if(is_page('careers')){
	    	$description = get_field('introduction_description', $post_id);
	    	$image_url = get_field('introduction_image', $post_id);
	    }else if(is_page('contact')){
	    	$description = get_field('contact_info_description', $post_id);
	    }else if(is_page('application')){
	    	$position_query = isset($_GET['position']) ? $_GET['position'] : false;
			$location_query = isset($_GET['location']) ? $_GET['location'] : false;

			if($position_query){
				$title = 'IES | ' . $position_query;
				$url = add_query_arg('position', $position_query, get_permalink());
			}

			if($location_query){
				$title .= " (" . $location_query . ")";
				$url = add_query_arg('location', $location_query, $url);
			}

	    	$description = "We’re waiting to work with you";
	    }
	}else if(is_single()){
		$post_id = $post->ID;

		$post_type = get_post_type();

		if($post_type == 'project'){
	    	$description = get_field('short_description', $post_id);
	    	$image_url = get_field('image', $post_id);
	    }else if($post_type == 'news'){
	    	$description = get_field('short_description', $post_id);
	    	$image_url = get_field('image', $post_id);
	    }
	}

	$description = $description == '' ? $title : $description;

    echo '<meta property="og:title" content="' . $title . '"/>';
	echo '<meta property="og:description" content="' . $description . '"/>';
	echo '<meta property="og:type" content="website"/>';
	echo '<meta property="og:url" content="' . $url . '"/>';
	echo '<meta property="og:site_name" content="Impact Electrons"/>';
	echo '<meta property="og:image" content="' . $image_url . '"/>';
}

add_action( 'wp_head', 'insert_fb_in_head', 5 );