var $filterView = false;
var move = 200;
var sliderLimit = -750;
var filterWidth = 0;
var filterItemWidth = 0;

$(function(){
	$("#done-project-load-more-btn").click(function(){
		var url = $(this).attr('data-href');
		var loadedIds = [];

		$("#what-we-have-done .project-list .project-item").each(function(){
			var itemId = $(this).attr('data-id');

			loadedIds.push(itemId);
		});

		$.ajax({
			url: url,
			method: "post",
			data: { content_type : "done-project", loaded_ids : loadedIds },
			dataType: "json"
		}).done(function(response){
			if(response.result == false){
		        var message = response.message;

		        $('#message-dialog .modal-body').html(message);

		        // Display message dialog
		        $('#message-dialog').modal();
		    }else{
		    	var message = response.message;
		    	var data = response.data;
		    	var allAreLoaded = response.all_are_loaded;
		    	// var redirectUrl = response.redirect_url;

		    	$("#what-we-have-done .project-list").append(data);
		    	onScrollInit( $('.animate') );

		    	if(allAreLoaded){
		    		$("#done-project-load-more-btn").hide();
		    	}
		    }
		});
	});

	$("#ongoing-project-load-more-btn").click(function(){
		var url = $(this).attr('data-href');
		var loadedIds = [];

		$("#what-we-are-doing .project-list .project-item").each(function(){
			var itemId = $(this).attr('data-id');

			loadedIds.push(itemId);
		});

		$.ajax({
			url: url,
			method: "post",
			data: { content_type : "ongoing-project", loaded_ids : loadedIds },
			dataType: "json"
		}).done(function(response){
			if(response.result == false){
		        var message = response.message;

		        $('#message-dialog .modal-body').html(message);

		        // Display message dialog
		        $('#message-dialog').modal();
		    }else{
		    	var message = response.message;
		    	var data = response.data;
		    	var allAreLoaded = response.all_are_loaded;
		    	// var redirectUrl = response.redirect_url;

		    	$("#what-we-are-doing .project-list").append(data);
		    	onScrollInit( $('.animate') );

		    	$('#what-we-are-doing .project-list > .project-item.new').matchHeight();

		    	if($(".project-title").length > 0){
					$(".project-title").truncate({
					  	lines: 2
					});
				}

				$('#what-we-are-doing .project-list > .project-item.new').removeClass('new');

		    	if(allAreLoaded){
		    		$("#ongoing-project-load-more-btn").hide();
		    	}
		    }
		});
	});

	$filterView = $('#project-filters');
	filterWidth = $filterView.outerWidth();

	$('#project-filters .project-filter-item').each(function(){
		filterItemWidth += $(this).outerWidth();
	});

	if($('#project-filters .project-filter-item').length > 1){
		// Plus margin
		var mlSpace = parseInt($('#project-filters .project-filter-item').eq(1).css('margin-left')) + 5;
		var totalSpace = $('#project-filters .project-filter-item').length - 1;

		filterItemWidth += mlSpace * totalSpace;
	}

	if(filterItemWidth < filterWidth){
		$("#project-filters-container .nav-button").addClass('hide');
	}

	$(window).resize(function(){
		filterWidth = $filterView.outerWidth();

		if(filterItemWidth < filterWidth){
			$("#project-filters-container .nav-button").addClass('hide');
		}else{
			$("#project-filters-container .nav-button").removeClass('hide');
		}
	});

	$('#project-filters').scroll(function(){
		if($(this).scrollLeft() <= 0){
			$("#project-filters-container .nav-button.prev").addClass('disabled');
		}else{
			$("#project-filters-container .nav-button.prev").removeClass('disabled');
		}

		var currentPosition = $(this).scrollLeft();
	    var scrollWidth = $(this).get(0).scrollWidth;

		if(scrollWidth - currentPosition <= filterWidth){
	    	$("#project-filters-container .nav-button.next").addClass('disabled');
	    }else{
	    	$("#project-filters-container .nav-button.next").removeClass('disabled');
	    }
	});

	var activeOffsetLeft = $('#project-filters > li.active').offset().left - $('#project-filters').offset().left;
	$('#project-filters').scrollLeft(activeOffsetLeft);

	$('#what-we-are-doing .project-list .project-item').matchHeight();

	// Initialise project filter nav buttons
	$("#project-filters-container .nav-button.prev").click(function(){
	    var currentPosition = $filterView.scrollLeft();
	    var targetPosition = currentPosition - move;
	    targetPosition = targetPosition < 0 ? 0 : targetPosition;
	    var isDisabled = false;

	    if(targetPosition == 0){
	    	isDisabled = true;
	    }

	    $filterView.animate({
	    	scrollLeft: targetPosition + "px"
	    },{ duration: 200 });

	    if(isDisabled){
    		$(this).addClass('disabled');
    	}else{
    		$(this).removeClass('disabled');
    	}

    	if(targetPosition != currentPosition){
    		$("#project-filters-container .nav-button.next").removeClass('disabled');
    	}
	});

	$("#project-filters-container .nav-button.next").click(function(){
	    var currentPosition = $filterView.scrollLeft();
	    var scrollWidth = $filterView.get(0).scrollWidth;
	    var targetPosition = currentPosition + move;
	    targetPosition = targetPosition > filterItemWidth ? filterItemWidth : targetPosition;
	    var isDisabled = false;

	    if(scrollWidth - targetPosition <= filterWidth){
	    	isDisabled = true;
	    }

	    $filterView.animate({
	    	scrollLeft: targetPosition + "px"
	    },{ duration: 200 });

	    if(isDisabled){
    		$(this).addClass('disabled');
    	}else{
    		$(this).removeClass('disabled');
    	}

    	if(targetPosition != currentPosition){
    		$("#project-filters-container .nav-button.prev").removeClass('disabled');
    	}
	});

	if(filterItemWidth < filterWidth){
		$("#project-filters-container .nav-button.next").addClass('disabled');
	}
});