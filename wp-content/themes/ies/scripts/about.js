$(function(){
	initMagicNumberPanel();

	$('.staff-list > li').matchHeight();
});

function initMagicNumberPanel(){
    $('.magic-number-col').matchHeight();

	var $numberPanel = $('#ies-in-number');
	var osAnimationClass = $numberPanel.attr('data-os-animation');
	var osAnimationDelay = $numberPanel.attr('data-os-animation-delay');

	$numberPanel.css({
        '-webkit-animation-delay':  osAnimationDelay,
        '-moz-animation-delay':     osAnimationDelay,
        'animation-delay':          osAnimationDelay,
        'opacity': 					0
    });

	$('#ies-in-number').waypoint(function(){
		var element = $(this)[0].element;
		$(element).addClass('animated').addClass(osAnimationClass);
        initNumbers();
        this.destroy();
    },{
        offset: '90%'
    });
}