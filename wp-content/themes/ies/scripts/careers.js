$(function(){
	$('#view-position-btn').click(function(){
		var target = $(this).attr('data-href');
		var windowWidth = $(window).width();
		var isMobile = windowWidth < 768;
		var offset = isMobile ? 80 : 40;

		$('html, body').animate({
          	scrollTop: $(target).offset().top - offset
        }, 1000);
	});

	initTimerSlide();
});

function initTimerSlide(){
    if($('.owl-carousel').length <= 0){
        return;
    }

    var staffTime = 5; // time in seconds

    var taffIsPause,
        staffTick,
        staffPercentTime;

    $('.owl-carousel').each(function(){
        var loop = $(this).find('.item').length > 1;
        var maxItems = Math.min(2, $(this).find('.item').length);

        $(this).owlCarousel({
            loop: loop,
            margin: 10,
            nav: true,
            dots: true,
            navText: '',
            navSpeed : 500,
            dotSpeed : 500,
            autoplayHoverPause: true,
            responsive:{
                0:{
                    items: 1,
                    slideBy: 1
                },
                768:{
                    items: maxItems,
                    slideBy: maxItems
                },
                1000:{
                    items: maxItems,
                    slideBy: maxItems
                }
            },
            onInitialized: function(event){
                progressBar(event);

                var $elem = $(event.target);
                var t = this,
                    currSlide = t._current + 1 - (t._items.length - 1),
                    length = t._items.length;
                var $slideNav = $elem.find('.owl-nav > .owl-prev');

                $('<div class="slide-num"><span class="text-bold">' + currSlide + '</span> / ' + length + '</div>').insertAfter($slideNav);
            },
            onTranslated:  function(event){
                moved(event);

                var $elem = $(event.target);
                var t = this,
                    currSlide = t._current + 1 - (t._items.length - 1),
                    length = t._items.length;

                if(currSlide <= 0){
                    currSlide = length;
                }else if(currSlide > length){
                    currSlide = currSlide % length;
                }

                $elem.find('.owl-nav > div.slide-num').html('<span class="text-bold">' + currSlide + '</span> / ' + length);
            },
            onDrag : pauseOnDragging
        });
    });

    $(document).on("mouseenter", ".owl-carousel", function() {
        pauseOnHovering(event);
    });

    $(document).on("mouseleave", ".owl-carousel", function() {
        resumeOnHovering(event);
    });

    // Init progressBar where elem iss $("#owl-demo")
    function progressBar(event){
        var $elem = $(event.target);
        $elem.find('.owl-dot .progress-bar').remove();

        // build progress bar elements
        buildProgressBar($elem);
        // start counting
        start($elem);
    }

    // Create div#progressBar and div#bar then prepend to $("#owl-demo")
    function buildProgressBar($elem){
        var $progressBar = $("<div>",{
            class: "progress-bar"
        });

        var $bar = $("<div>",{
            class: "bar"
        });

        $progressBar.append($bar).prependTo($elem.find('.owl-dot.active'));
    }

    function start($elem) {
        // Reset timer
        if($elem.attr('id') == "staff-voice-list"){
            staffIsPause = false;
            staffPercentTime = 0;

            // Run interval every 0.01 second
            staffTick = setInterval(function(){ interval($elem); }, 10);
        }
    };

    function interval($elem) {
        if($elem.attr('id') == "staff-voice-list"){
            if(staffIsPause === false){
                var $bar = $elem.find('.owl-dot.active .bar');

                staffPercentTime += 1 / staffTime;

                $bar.css({
                    width: staffPercentTime + "%"
                });

                // If percentTime is equal or greater than 100
                if(staffPercentTime >= 100){
                    // Slide to next item
                    $elem.trigger('next.owl.carousel');
                    staffPercentTime = -100;
                }
            }
        }
    }

    // Pause while dragging
    function pauseOnDragging(event){
        // isPause = true;
        var $elem = $(event.target);

        // Clear interval
        if($elem.attr('id') == "staff-voice-list"){
            staffIsPause = true;
        }
    }

    // Moved callback
    function moved(event){
        var $elem = $(event.target);

        // Clear interval
        if($elem.attr('id') == "staff-voice-list"){
            clearTimeout(staffTick);
        }

        // Start again
        progressBar(event);
    }

    // Pause while hovering
    function pauseOnHovering(event){
        var $target = $(event.target);
        var $elem = $target.parents('.owl-carousel');

        // Clear interval
        if($elem.attr('id') == "staff-voice-list"){
            staffIsPause = true;
        }
    }

    // Pause while hovering
    function resumeOnHovering(event){
        var $target = $(event.target);
        var $elem = $target.parents('.owl-carousel');

        // Clear interval
        if($elem.attr('id') == "staff-voice-list"){
            staffIsPause = false;
        }
    }
}