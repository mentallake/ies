$(function(){
	// $('#cv-upload-btn').click(function(){
	// 	$("#cv-input-file").click();
	// });

	$("#cv-input-file").change(function(){
		var fullPath = $(this).val();

		var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
	    var filename = fullPath.substring(startIndex);
	    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
	        filename = filename.substring(1);
	    }

		$('#cv-filename').text(filename);
	});

	$('#application-form').submit(function(){
		if(!isFieldCompleted('#application-form')){
			return false;
		}

		var careersPageUrl = $('#careers-page-url').val();
		var options = {
	        success: showResponse,
	        dataType: 'json',
	        data: { }
	    };

	    $(this).ajaxSubmit(options);

	    return false;
	});
});

function showResponse(response, statusText, xhr, $form)  {
	if(response.result == false){
        var message = response.message;

        // $('#message-dialog').modal('hide');

        $('#message-dialog .modal-body').html(message);

        // hideLoadingPanel();

        // Display message dialog
        $('#message-dialog').modal();
    }else{
    	// hideLoadingPanel();

    	var message = response.message;
    	var redirectUrl = response.redirect_url;

    	// $('#message-dialog').modal('hide');
        // $('#message-dialog .modal-body').html(message);

        // $('#message-dialog #ok-button').click(function(){
        // 	window.location.href = redirectUrl;
        // });

        // $('#message-dialog').modal();

        window.location.href = redirectUrl;
    }
}