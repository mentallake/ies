$(function(){
	$("#normal-news-load-more-btn").click(function(){
		var url = $(this).attr('data-href');
		var loadedIds = [];

		$("#latest-news .news-list .news-item").each(function(){
			var newsId = $(this).attr('data-id');

			loadedIds.push(newsId);
		});

		$.ajax({
			url: url,
			method: "post",
			data: { content_type : "news", loaded_ids : loadedIds },
			dataType: "json"
		}).done(function(response){
			if(response.result == false){
		        var message = response.message;

		        $('#message-dialog .modal-body').html(message);

		        // Display message dialog
		        $('#message-dialog').modal();
		    }else{
		    	var message = response.message;
		    	var data = response.data;
		    	var allAreLoaded = response.all_are_loaded;
		    	// var redirectUrl = response.redirect_url;

		    	$("#latest-news .news-list").append(data);
		    	onScrollInit( $('.animate') );

		    	$('#latest-news .news-list > .news-item.new').matchHeight();

		    	if($(".news-title").length > 0){
					$(".news-title").truncate({
					  	lines: 3
					});
				}

				$('#latest-news .news-list > .news-item.new').removeClass('new');

		    	if(allAreLoaded){
		    		$("#normal-news-load-more-btn").hide();
		    	}
		    }
		});
	});

	$('#latest-news .news-list > .news-item').matchHeight();
});