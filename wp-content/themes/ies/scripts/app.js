$(function(){
	initAnimateCss();
	initDesktopHeaderPanel();
	initMobileHeaderPanel();
	onScrollInit( $('.animate') );

	initElements();
});

function initElements(){
	if(!$("body").hasClass('home')){
		$('#header-panel').css('opacity', 1);
	}

	$('.qtranxs-lang-menu').hover(function(){
		var windowWidth = $(window).width();

		if(windowWidth >= 768){
			$(this).find('> a').click();
		}
	});

	$('.qtranxs-lang-menu').click(function(e){
		var windowWidth = $(window).width();
		var $target = $(e.target);

		if(windowWidth >= 768){
			if(e.hasOwnProperty('originalEvent')){
		        // Probably a real click.

		        if(!$target.is('.qtranxs-lang-menu-item > a')){
		        	return false;
		        }else{
		        	$('.qtranxs-lang-menu').removeClass('open');
		        	$('.qtranxs-lang-menu .dropdown-menu').addClass('hide');
		        }
			}else{
		        // Probably a fake click.
			}
		}
	});

	$(window).scroll(function(){
		var currentScroll = $(this).scrollTop();

	   	if (currentScroll >= 200){
	   		$('#go-to-top-btn').fadeIn();
	   	}else{
	   		$('#go-to-top-btn').fadeOut();
	   	}
	});

	$('#go-to-top-btn').click(function(){
		$('html, body').animate({
          	scrollTop: 0
        }, 1000);
	});

	$('#menu-main-menu > li').each(function(){
		var menuTitle = $(this).find('a').attr('title');
		$(this).find('a').attr('title', '');
		$(this).find('a').attr('data-title', menuTitle);
	});

	var languageCode = $('#language-code').val();
	$('.qtranxs-lang-menu > .dropdown-toggle').html(languageCode.toUpperCase());
	$('.qtranxs-lang-menu-item .glyphicon.' + languageCode.toUpperCase()).parents('.qtranxs-lang-menu-item').addClass('active');

	$('.number-only').keypress(function(evt){
	    if (evt.which < 48 || evt.which > 57){
	        evt.preventDefault();
	    }
	});

	$('.phone-keyboard-only').keypress(function(e){
		// Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: #, *, +
            (e.keyCode == 35 || e.keyCode == 42 || e.keyCode == 43)) {
                 // let it happen, don't do anything
                 return;
        }

        // Ensure that it is a number and stop the keypress
        if (e.keyCode < 48 || e.keyCode > 57) {
            e.preventDefault();
        }
	});

	$(window).load(function(){
		if($(".news-title").length > 0){
			$(".news-title").truncate({
			  	lines: 3
			});
		}

		if($(".project-short-description").length > 0){
			$(".project-short-description").truncate({
			  	lines: 4
			});
		}
	});

	$(window).resize(function(){
		if($(".news-title").length > 0){
			$(".news-title").truncate('config', {
			  	lines: 3
			});
		}

		if($(".project-short-description").length > 0){
			$(".project-short-description").truncate('config', {
			  	lines: 4
			});
		}
	});

	$('#show-all-factsheets').click(function(){
		$("#download-factsheet-dialog").modal();
		// $('#footer-factsheet-list > li.hide').animateCss('fadeIn', 'in');
		// $(this).hide();
	});
}

var lastScrollTop = 0;

function initDesktopHeaderPanel(){
	var offset = 120;
	var transitionTime = 200;

	// var windowWidth = $(window).width();

	// if(windowWidth < 768){
	// 	$('#header-panel').css('height', 'auto');
	// 	return;
	// }

	$(window).resize(function(){
		var windowWidth = $(window).width();

		if(windowWidth < 768){
			$('#header-panel').css('height', 'auto');
			return;
		}
	});

	$(window).scroll(function(){
		// var windowWidth = $(window).width();

		// if(windowWidth < 768){
		// 	$('#header-panel').css('height', 'auto');
		// 	return;
		// }

		var currentScroll = $(this).scrollTop();

	   	if (currentScroll >= lastScrollTop){
		    // downscroll code
		    lastScrollTop = currentScroll;

		    if(currentScroll > offset){
				if($('#header-panel').hasClass('shrink')){
					return;
				}

				$('#header-panel').addClass('shrink');
				$('#header-panel').animate({

				}, transitionTime, function(){

				});
			}
			else{
				if(!$('#header-panel').hasClass('shrink')){
					return;
				}

				$('#header-panel').removeClass('shrink');
				$('#header-panel').animate({

				}, transitionTime, function(){

				});
			}
		} else {
		    // upscroll code
		    lastScrollTop = currentScroll;

		    if(!$('#header-panel').hasClass('shrink')){
				return;
			}

			$('#header-panel').removeClass('shrink');
			$('#header-panel').animate({

			}, transitionTime, function(){

			});
		}
	});
}

function initMobileHeaderPanel(){
	$('.navbar-toggle').click(function(){
		$('#header-panel').toggleClass('open');
		$('#header-panel .menu').toggleClass('open');
	});
}

function onScrollInit( items, trigger ) {
  	items.each( function() {
	    var osElement = $(this),
	        osAnimationClass = osElement.attr('data-os-animation'),
	        osAnimationDelay = osElement.attr('data-os-animation-delay');

	    osElement.css({
	        '-webkit-animation-delay':  osAnimationDelay,
	        '-moz-animation-delay':     osAnimationDelay,
	        'animation-delay':          osAnimationDelay,
	        'opacity': 					0
	    });

	    var osTrigger = ( trigger ) ? trigger : osElement;

	    osTrigger.waypoint(function() {
	        osElement.addClass('animated').addClass(osAnimationClass);
	        osElement.css('opacity', '1');
	        this.destroy();
	    },{
	        offset: '120%'
	    });
	});
}

function initAnimateCss(){
	$.fn.extend({
	    animateCss: function (animationName, type) {
	    	if(type == 'in'){
            	$(this).removeClass('hide');
            	$(this).css('opacity', '1');
            }

	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
	            $(this).removeClass('animated ' + animationName);

	            if(type == 'out'){
	            	$(this).css('opacity', '0');
	            	$(this).addClass('hide');
	            }
	        });
	    }
	});
}

function isFieldCompleted(panelId){
	var $panel = $(panelId);
	var isFieldCompleted = true;

	$panel.find('.error').removeClass('error');

	$panel.find('.required').each(function(){
        // If it is textbox, textarea.
        if($(this).is('input, textarea')){
            if($(this).val() == ''){
                isFieldCompleted = false;

                if($(this).parent().hasClass('input-file-wrapper')){
                	$(this).parent().addClass('error');
                }else{
                	$(this).addClass('error');
                }
            }
        }
    });

    $panel.find('[type="email"]').each(function(){
        // If it is email.
        if($(this).is('input')){
            if(!validateEmail($(this).val())){
                isFieldCompleted = false;

                if($(this).parent().hasClass('input-file-wrapper')){
                	$(this).parent().addClass('error');
                }else{
                	$(this).addClass('error');
                }
            }
        }
    });

    if(!isFieldCompleted){
        $panel.find('.error').eq(0).focus();
        $panel.find('.error').animateCss('pulse', 'in');
    }

    return isFieldCompleted;
}

function validateEmail(email) {
  	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return re.test(email);
}

function initNumbers(){
	$('.magic-number').each(function(){
		var number = $(this).attr('data-value');
		var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
		$(this).animateNumber(
		  	{
		    	number: number,
	            numberStep: comma_separator_number_step
		  	}, 1500
		);
	});
}