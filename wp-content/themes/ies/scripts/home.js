$(function(){
    initSplashScreen();
    initHeaderPanel();
    initSlider();
    initTimerSlide();
    initWhatWedo();
    initFirstMovers();
    initMagicNumberPanel();
});

function initSplashScreen(){
    if($('#splashscreen').length > 0){
        $('#home-page').css('opacity', 1);
        $('#header-panel').css('opacity', 1);
        $('#splashscreen > img').animateCss('fadeIn', 'in');

        setTimeout(function(){
            $('.cd-image-container > img').on('load', function(){
                $('#splashscreen').animateCss('fadeOut', 'out');
                var slideTheme = $('#slide-theme').val();
                $('#header-panel').addClass(slideTheme);
                $('.cd-image-container').addClass('is-visible');
                $('#header-panel').css('opacity', 1);
            }).each(function() {
                if(this.complete){
                    $(this).load();
                }
            });

            // $('#splashscreen').animateCss('fadeOut', 'out');
            // var slideTheme = $('#slide-theme').val();
            // $('#header-panel').addClass(slideTheme);
            // $('.cd-image-container').addClass('is-visible');
            // $('#header-panel').css('opacity', 1);
        }, 2000);
    }else{
        var slideTheme = $('#slide-theme').val();
        $('#header-panel').addClass(slideTheme);
        // $('#header-panel').css('opacity', 1);

        $('.cd-image-container > img').on('load', function(){
            considerSliderHandlePosition();
            considerSliderHeight();

            $('.cd-image-container').addClass('is-visible');
        }).each(function() {
            if(this.complete){
                $(this).load();
            }
        });

        $(window).on('load', function(){
            $('#home-page').animateCss('fadeIn', 'in');
            $('#header-panel').css('opacity', 1);
        });
    }
}

function initWhatWedo(){
    var totalProjectTypes = $("#project-type-list .project-type").length;

    $(window).resize(function(){
        var windowWidth = $(window).width();

        if($("#project-type-list").length > 0){
            if(windowWidth > 768 || totalProjectTypes > 2){
                $("#project-type-list").addClass('owl-carousel owl-theme');
                $("#project-type-list").addClass('owl-carousel top-nav');

                $("#project-type-list").owlCarousel({
                    loop: true,
                    margin: 0,
                    padding: 0,
                    nav: true,
                    dots: true,
                    navSpeed : 500,
                    dotSpeed : 500,
                    navText: '',
                    autoplayHoverPause: true,
                    responsive:{
                        0:{
                            items: 2,
                            slideBy: 2
                        },
                        768:{
                            items: 3,
                            slideBy: 3
                        },
                        1000:{
                            items: 4,
                            slideBy: 4
                        }
                    },
                    onInitialized: function(event){
                        progressBar(event);

                        var $elem = $(event.target);
                        var offset = $elem.find('.owl-item:not(.cloned)').first().index();
                        var t = this,
                            currSlide = Math.ceil((t._current + 1 - offset) / 2),
                            length = Math.ceil(t._items.length / 2);
                        var $slideNav = $elem.find('.owl-nav > .owl-prev');

                        $('<div class="slide-num"><span class="text-bold">' + currSlide + '</span> / ' + length + '</div>').insertAfter($slideNav);
                    },
                    onTranslated:  function(event){
                        moved(event);

                        var $elem = $(event.target);
                        var offset = $elem.find('.owl-item:not(.cloned)').first().index();
                        var t = this,
                            currSlide = Math.ceil((t._current + 1 - offset) / 2),
                            length = Math.ceil(t._items.length / 2);

                        if(currSlide <= 0){
                            currSlide = length;
                        }else if(currSlide > length){
                            currSlide = currSlide % length;
                        }

                        $elem.find('.owl-nav > div.slide-num').html('<span class="text-bold">' + currSlide + '</span> / ' + length);
                    },
                    onDrag : pauseOnDragging
                });
            }else{
                $('#project-type-list').trigger('destroy.owl.carousel');
                $('#project-type-list').removeClass('owl-carousel');
            }
        }
    });

    $('#project-type-list .project-type > a').matchHeight();
}

function initFirstMovers(){
    $(window).resize(function(){
        var windowWidth = $(window).width();

        if(windowWidth < 768){
            $("#first-mover-container").addClass('owl-carousel');
            $("#first-mover-container").addClass('owl-carousel top-nav');

            $("#first-mover-container").owlCarousel({
                loop: true,
                items: 1,
                margin: 0,
                nav: true,
                dots: true,
                navSpeed : 500,
                dotSpeed : 500,
                navText: '',
                autoHeight: false,
                onInitialized: function(event){
                    progressBar(event);

                    var $elem = $(event.target);
                    $elem.find('.owl-item.cloned .first-mover-item').css('opacity', 1);
                    var offset = $elem.find('.owl-item:not(.cloned)').first().index();
                    var t = this,
                        currSlide = t._current + 1 - offset,
                        length = t._items.length;
                    var $slideNav = $elem.find('.owl-nav > .owl-prev');

                    $('<div class="slide-num"><span class="text-bold">' + currSlide + '</span> / ' + length + '</div>').insertAfter($slideNav);

                    var maxContentHeight = 100;

                    $('.first-mover-content').each(function(){
                        var hasButton = $(this).find('.button-panel').length > 0;
                        var $button = $(this).find('.button-panel');
                        var contentHeight = hasButton ? $(this).outerHeight() + parseInt($button.css('margin-bottom')) : $(this).outerHeight();

                        maxContentHeight = Math.max(maxContentHeight, contentHeight);
                    });

                    $('.first-mover-content').css('height', maxContentHeight + 'px');
                },
                onTranslated:  function(event){
                    moved(event);

                    var $elem = $(event.target);
                    var offset = $elem.find('.owl-item:not(.cloned)').first().index();
                    var t = this,
                        currSlide = t._current + 1 - offset,
                        length = t._items.length;

                    if(currSlide <= 0){
                        currSlide = length;
                    }else if(currSlide > length){
                        currSlide = currSlide % length;
                    }

                    $elem.find('.owl-nav > div.slide-num').html('<span class="text-bold">' + currSlide + '</span> / ' + length);
                },
                onDrag : pauseOnDragging
            })
        }else{
            $('.first-mover-content').css('height', 'auto');
            $('#first-mover-container').trigger('destroy.owl.carousel');
            $('#first-mover-container').removeClass('owl-carousel');

            // $('.first-mover-item > .row > div').matchHeight();

            $('.first-mover-col').matchHeight();
        }
    });

    $(window).resize();
}

function initHeaderPanel(){
    considerHeaderPanelBG();

    $(window).scroll(function(){
        considerHeaderPanelBG();
    });

    // Init additional action for home page on mobile.
    $('.navbar-toggle').click(function(){
        if($(this).hasClass('collapsed')){

        }else{
            considerHeaderPanelBG();
        }
    });
}

function considerHeaderPanelBG(){
    var offsetTop = $(window).scrollTop();
    var windowHeight = $(window).height();
    var percent = offsetTop / windowHeight * 5;

    if(percent > 0.5){
        if(percent >= 0.95)
            percent = 0.95;

        $('#header-panel > .navbar').css('background', 'rgba(255, 255, 255,' + percent + ')');

        if(percent >= 0.7){
            $('#header-panel').addClass('normal-logo');
        }else{
            $('#header-panel').removeClass('normal-logo');
        }
    }else{
        $('#header-panel > .navbar').css('background', 'rgba(255, 255, 255, 0)');
        $('#header-panel').removeClass('normal-logo');
    }
}

function initMagicNumberPanel(){
    $('.magic-number-col').matchHeight();

    var $numberPanel = $('#ies-in-number');
    var osAnimationClass = $numberPanel.attr('data-os-animation');
    var osAnimationDelay = $numberPanel.attr('data-os-animation-delay');

    $numberPanel.css({
        '-webkit-animation-delay':  osAnimationDelay,
        '-moz-animation-delay':     osAnimationDelay,
        'animation-delay':          osAnimationDelay,
        'opacity':                  0
    });

    $('#ies-in-number').waypoint(function(){
        var element = $(this)[0].element;
        $(element).addClass('animated').addClass(osAnimationClass);
        initNumbers();
        this.destroy();
    },{
        offset: '90%'
    });
}

function considerSliderHeight(){
    var imageHeight = $('.cd-image-container > img').height();
    var windowHeight = $(window).height();
    var targetHeight = Math.min(imageHeight, windowHeight);

    $('#slide-panel').css('height', targetHeight + 'px');
    $('.slide-content').css('height', targetHeight + 'px');
}

function considerSliderHandlePosition(){
    var buttonOffsetTop = $('#slide-button').offset().top;
    $('.cd-handle').css('top', buttonOffsetTop - 34 + 'px');
}

function initSlider(){
    $('.cd-image-container > img').load(function(){
        considerSliderHandlePosition();
        considerSliderHeight();
    });

    $(window).resize(function(){
        considerSliderHeight();

        setTimeout(function(){
            considerSliderHandlePosition();
        }, 50);
    });

    $(window).resize();

    $('.cd-image-container').each(function(){
        var actual = $(this);
        drags(actual.find('.cd-handle'), actual.find('.cd-resize-img'), actual);
    });
}

//draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
function drags(dragElement, resizeElement, container) {
    dragElement.on("mousedown vmousedown", function(e) {
        dragElement.addClass('draggable');
        resizeElement.addClass('resizable');

        var windowWidth = $(window).width();
        var offset = windowWidth < 768 ? 10 : 20;

        var dragWidth = dragElement.outerWidth(),
            xPosition = dragElement.offset().left + dragWidth - e.pageX,
            containerOffset = container.offset().left,
            containerWidth = container.outerWidth(),
            minLeft = containerOffset + offset,
            maxLeft = containerOffset + containerWidth - dragWidth - offset;

        dragElement.parents().on("mousemove vmousemove", function(e) {
            leftValue = e.pageX + xPosition - dragWidth;

            //constrain the draggable element to move inside its container
            if(leftValue < minLeft ) {
                leftValue = minLeft;
            } else if ( leftValue > maxLeft) {
                leftValue = maxLeft;
            }

            widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';

            $('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
                $(this).removeClass('draggable');
                resizeElement.removeClass('resizable');
            });

            $('.resizable').css('width', widthValue);

            //function to upadate images label visibility here
            // ...

        }).on("mouseup vmouseup", function(e){
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
        e.preventDefault();
    }).on("mouseup vmouseup", function(e) {
        dragElement.removeClass('draggable');
        resizeElement.removeClass('resizable');
    });
}

var whatWeDoTime = 5; // time in seconds
var firstMoverTime = 10; // time in seconds
var customerTime = 4; // time in seconds
var staffTime = 5; // time in seconds

var whatWeDoIsPause,
    whatWeDoTick,
    whatWeDoPercentTime,
    firstMoverIsPause,
    firstMoverTick,
    firstMoverPercentTime,
    customerIsPause,
    customerTick,
    customerPercentTime,
    staffIsPause,
    staffTick,
    staffPercentTime;

function initTimerSlide(){
    if($('.owl-carousel').length <= 0){
        return;
    }

    $('.owl-carousel').each(function(){
        var loop = $(this).find('.item').length > 1;
        var maxItems = Math.min(2, $(this).find('.item').length);

        $(this).owlCarousel({
            loop: loop,
            margin: 10,
            nav: true,
            dots: true,
            navSpeed : 500,
            dotSpeed : 500,
            navText: '',
            autoplayHoverPause: true,
            responsive:{
                0:{
                    items: 1,
                    slideBy: 1
                },
                768:{
                    items: maxItems,
                    slideBy: maxItems
                },
                1000:{
                    items: maxItems,
                    slideBy: maxItems
                }
            },
            onInitialized: function(event){
                progressBar(event);

                var $elem = $(event.target);
                var offset = $elem.find('.owl-item:not(.cloned)').first().index();
                var t = this,
                    currSlide = t._current + 1 - offset,
                    length = t._items.length;
                var $slideNav = $elem.find('.owl-nav > .owl-prev');

                $('<div class="slide-num"><span class="text-bold">' + currSlide + '</span> / ' + length + '</div>').insertAfter($slideNav);
            },
            onTranslated:  function(event){
                moved(event);

                var $elem = $(event.target);
                var offset = $elem.find('.owl-item:not(.cloned)').first().index();
                var t = this,
                    currSlide = t._current + 1 - offset,
                    length = t._items.length;

                if(currSlide <= 0){
                    currSlide = length;
                }else if(currSlide > length){
                    currSlide = currSlide % length;
                }

                $elem.find('.owl-nav > div.slide-num').html('<span class="text-bold">' + currSlide + '</span> / ' + length);
            },
            onDrag : pauseOnDragging
        });
    });

    $(document).on("mouseenter", ".owl-carousel", function() {
        pauseOnHovering(event);
    });

    $(document).on("mouseleave", ".owl-carousel", function() {
        resumeOnHovering(event);
    });
}

// Init progressBar where elem iss $("#owl-demo")
function progressBar(event){
    var $elem = $(event.target);
    $elem.find('.owl-dot .progress-bar').remove();

    // build progress bar elements
    buildProgressBar($elem);
    // start counting
    start($elem);
}

// Create div#progressBar and div#bar then prepend to $("#owl-demo")
function buildProgressBar($elem){
    var $progressBar = $("<div>",{
        class: "progress-bar"
    });

    var $bar = $("<div>",{
        class: "bar"
    });

    $progressBar.append($bar).prependTo($elem.find('.owl-dot.active'));
}

function start($elem) {
    var windowWidth = $(window).width();

    // Reset timer
    if($elem.attr('id') == "project-type-list"){
        whatWeDoIsPause = $("#project-type-list:hover").length > 0;

        if(windowWidth < 768){
            whatWeDoIsPause = false;
        }

        whatWeDoPercentTime = 0;
        $(document).mouseenter();

        // Run interval every 0.01 second
        whatWeDoTick = setInterval(function(){ interval($elem); }, 10);
    }else if($elem.attr('id') == "first-mover-container"){
        firstMoverIsPause = $("#first-mover-container:hover").length > 0;

        if(windowWidth < 768){
            firstMoverIsPause = false;
        }

        firstMoverPercentTime = 0;

        // Run interval every 0.01 second
        firstMoverTick = setInterval(function(){ interval($elem); }, 10);
    }else if($elem.attr('id') == "customer-trust-list"){
        customerIsPause = $("#customer-trust-list:hover").length > 0;

        if(windowWidth < 768){
            customerIsPause = false;
        }

        customerPercentTime = 0;

        // Run interval every 0.01 second
        customerTick = setInterval(function(){ interval($elem); }, 10);
    }else if($elem.attr('id') == "staff-voice-list"){
        staffIsPause = $("#staff-voice-list:hover").length > 0;

        if(windowWidth < 768){
            staffIsPause = false;
        }

        staffPercentTime = 0;

        // Run interval every 0.01 second
        staffTick = setInterval(function(){ interval($elem); }, 10);
    }

    if(windowWidth < 768){
        whatWeDoIsPause = false;
    }
};

function interval($elem) {
    if($elem.attr('id') == "project-type-list"){
        if(whatWeDoIsPause === false){
            var $bar = $elem.find('.owl-dot.active .bar');

            whatWeDoPercentTime += 1 / whatWeDoTime;

            $bar.css({
                width: whatWeDoPercentTime + "%"
            });

            // If percentTime is equal or greater than 100
            if(whatWeDoPercentTime >= 100){
                // Slide to next item
                $elem.trigger('next.owl.carousel');
                whatWeDoPercentTime = -100;
            }
        }
    }else if($elem.attr('id') == "first-mover-container"){
        if(firstMoverIsPause === false){
            var $bar = $elem.find('.owl-dot.active .bar');

            firstMoverPercentTime += 1 / firstMoverTime;

            $bar.css({
                width: firstMoverPercentTime + "%"
            });

            // If percentTime is equal or greater than 100
            if(firstMoverPercentTime >= 100){
                // Slide to next item
                $elem.trigger('next.owl.carousel');
                firstMoverPercentTime = -100;
            }
        }
    }else if($elem.attr('id') == "customer-trust-list"){
        if(customerIsPause === false){
            var $bar = $elem.find('.owl-dot.active .bar');

            customerPercentTime += 1 / customerTime;

            $bar.css({
                width: customerPercentTime + "%"
            });

            // If percentTime is equal or greater than 100
            if(customerPercentTime >= 100){
                // Slide to next item
                $elem.trigger('next.owl.carousel');
                customerPercentTime = -100;
            }
        }
    }else if($elem.attr('id') == "staff-voice-list"){
        if(staffIsPause === false){
            var $bar = $elem.find('.owl-dot.active .bar');

            staffPercentTime += 1 / staffTime;

            $bar.css({
                width: staffPercentTime + "%"
            });

            // If percentTime is equal or greater than 100
            if(staffPercentTime >= 100){
                // Slide to next item
                $elem.trigger('next.owl.carousel');
                staffPercentTime = -100;
            }
        }
    }
}

// Pause while dragging
function pauseOnDragging(event){
    var $elem = $(event.target);
    var $currentElem = $(event.currentTarget);

    // Clear interval
    if($elem.attr('id') == "project-type-list"){
        whatWeDoIsPause = true;
    }else if($elem.attr('id') == "first-mover-container"){
        firstMoverIsPause = true;
    }else if($elem.attr('id') == "customer-trust-list"){
        customerIsPause = true;
    }else if($elem.attr('id') == "staff-voice-list"){
        staffIsPause = true;
    }
}

// Pause while hovering
function pauseOnHovering(event){
    var $target = $(event.target);
    var $elem = $target.parents('.owl-carousel');

    // Clear interval
    if($elem.attr('id') == "project-type-list"){
        whatWeDoIsPause = true;
    }else if($elem.attr('id') == "first-mover-container"){
        firstMoverIsPause = true;
    }else if($elem.attr('id') == "customer-trust-list"){
        customerIsPause = true;
    }else if($elem.attr('id') == "staff-voice-list"){
        staffIsPause = true;
    }
}

// Pause while hovering
function resumeOnHovering(event){
    var $target = $(event.target);
    var $elem = $target.parents('.owl-carousel');

    // Clear interval
    if($elem.attr('id') == "project-type-list"){
        whatWeDoIsPause = false;
    }else if($elem.attr('id') == "first-mover-container"){
        firstMoverIsPause = false;
    }else if($elem.attr('id') == "customer-trust-list"){
        customerIsPause = false;
    }else if($elem.attr('id') == "staff-voice-list"){
        staffIsPause = false;
    }
}

// Moved callback
function moved(event){
    var $elem = $(event.target);

    // Clear interval
    if($elem.attr('id') == "project-type-list"){
        clearTimeout(whatWeDoTick);
    }else if($elem.attr('id') == "first-mover-container"){
        clearTimeout(firstMoverTick);
    }else if($elem.attr('id') == "customer-trust-list"){
        clearTimeout(customerTick);
    }else if($elem.attr('id') == "staff-voice-list"){
        clearTimeout(staffTick);
    }

    // Start again
    progressBar(event);
}