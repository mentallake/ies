<?php
/**
 * The careers template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;
$post_slug = $post->post_name;
$page_title = get_the_title();
$staff_voices = get_field('staff_voices');
$people_images = get_field('people_images');
$opening_position_title = get_field('opening_position_title');
$opening_positions = get_field('opening_positions');
$people_title = get_field('people_title');
$job_description_title = get_field('job_description_title');
$qualification_description_title = get_field('qualification_description_title');
$apply_text = get_field('apply_text');
$apply_now_text = get_field('apply_now_text');

// Get application page
$application_page = get_page_by_path( 'application' );
$application_page_id = $application_page->ID;
$application_page_url = get_page_link($application_page_id);
?>

<div id="careers-page" class="content-page">
	<section id="career-intro"
			 class="content-section big-top-space animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="row">
				<div class="col-sm-5 animate"
					 data-os-animation="fadeIn"
	 		 		 data-os-animation-delay="0.4s">
					<div class="page-title"><?php the_field('page_title'); ?></div>
					<div class="section-title"><?php the_field('introduction_title'); ?></div>
					<p>
						<?php the_field('introduction_description'); ?>
					</p>

					<?php if(count($opening_positions) > 0 && $opening_positions){ ?>
					<div class="button-panel top-40">
						<a id="view-position-btn" data-href="#open-position" class="btn btn-rounded btn-red-1 medium"><?php the_field('position_button_text'); ?></a>
					</div>
					<?php } ?>
				</div>
				<div class="col-sm-7">
					<?php $image = get_field('introduction_image'); ?>
					<div id="career-image" style="background-image: url(<?php echo $image; ?>);"></div>
				</div>
			</div>
		</div>
	</section>

	<section id="our-brand"
			 class="content-section animate top-nav"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title text-center">
						<?php echo $people_title; ?>
					</div>
				</div>
			</div>

			<?php if($staff_voices && count($staff_voices) > 0){ ?>
			<div id="staff-voice-panel" class="">
				<div id="staff-voice-list" class="timer staff-voice-list row owl-carousel owl-theme">
					<?php
					global $post;

					$s_count = 0;
					$s_limit = 10;

					foreach ($staff_voices as $post) : setup_postdata($post);
						$s_count++;

						if($s_count > $s_limit){
							break;
						}

						if(!isset($post['staff_voice']) || !isset($post['staff_voice']->ID)){
							continue;
						}

						$p_id = $post['staff_voice']->ID;
						$firstname = get_field("staff_firstname", $p_id);
						$lastname = get_field("staff_lastname", $p_id);
						$name = $firstname . ' ' . $lastname;
						$position = get_field("role_title", $p_id);
						$voice = get_field("staff_voice", $p_id);
						$image = get_field("image", $p_id);
					?>
					<div class="staff-voice item">
						<div class="text">“<?php echo $voice; ?>”</div>
						<div class="staff-profile">
							<div class="profile-thumbnail" style="background-image: url(<?php echo $image; ?>);"></div>
							<div class="profile-content">
								<div class="name"><?php echo $name; ?></div>
								<div class="position"><?php echo $position; ?></div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
			<?php } ?>
		</div>
	</section>

	<?php if(count($people_images) > 0 && $people_images){ ?>
	<section id="career-image-panel"
			 class="animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container-fluid">
			<ul id="career-images" class="row">
				<?php
				$total_images = count($people_images);
				$image_classes = array();

				switch($total_images){
					case 1:
						$image_classes = array(
							"col-xs-12 col-sm-12"
						);
						break;
					case 2:
						$image_classes = array(
							"col-xs-6 col-sm-6", "col-xs-6 col-sm-6"
						);
						break;
					case 3:
						$image_classes = array(
							"col-xs-4 col-sm-4", "col-xs-4 col-sm-4", "col-xs-4 col-sm-4"
						);
						break;
					case 4:
						$image_classes = array(
							"col-xs-7 col-sm-7", "col-xs-5 col-sm-5",
							"col-xs-6 col-sm-6", "col-xs-6 col-sm-6"
						);
						break;
					case 5:
						$image_classes = array(
							"col-xs-5 col-sm-5", "col-xs-4 col-sm-4", "col-xs-3 col-sm-3",
							"col-xs-6 col-sm-6", "col-xs-6 col-sm-6"
						);
						break;
					case 6:
						$image_classes = array(
							"col-xs-5 col-sm-5", "col-xs-4 col-sm-4", "col-xs-3 col-sm-3",
							"col-xs-4 col-sm-4", "col-xs-4 col-sm-4", "col-xs-4 col-sm-4"
						);
						break;
				}

				$time = 0.3;

				for($i = 0; $i < $total_images; $i++){
					$p_image = $people_images[$i];
					$image_url = $p_image['url'];
					$col_class = $image_classes[$i];
					$time += 0.15;
				?>
				<li class="<?php echo $col_class; ?> animate"
					data-os-animation="fadeIn"
	 		 		data-os-animation-delay="<?php echo $time; ?>s"
					style="background-image: url(<?php echo $image_url; ?>);"></li>
				<?php } ?>
			</ul>
		</div>
	</section>
	<?php } ?>

	<?php if(count($opening_positions) > 0 && $opening_positions){ ?>
	<section id="open-position"
			 class="animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container">
			<div class="section-title text-center small"><?php echo $opening_position_title; ?></div>

			<div class="panel-group position-panel-group" id="position-accordion" role="tablist" aria-multiselectable="true">
				<?php
				global $post;

				$count = 0;

				while( have_rows('opening_positions') ) : the_row();
					$p_title = get_sub_field('position_title');
					$p_loaction = get_sub_field('location');
					$job_description = get_sub_field('job_description');
					$qualification_description = get_sub_field('qualification_description');
					$count++;
					$p_application_page_url = $application_page_url . "?position=" . $p_title . "&location=" . $p_loaction;
				?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="postion-1">
						<h4 class="panel-title">
							<div class="row narrow">
								<div class="col-sm-9 position-title-col">
									<a role="button" data-toggle="collapse" data-parent="#position-accordion" href="#position-detail-<?php echo $count; ?>" aria-expanded="true" aria-controls="position-detail-<?php echo $count; ?>" class="collapsed">
										<div class="position-title"><?php echo $p_title; ?></div>
										<div class="position-location"><?php echo $p_loaction; ?></div>
									</a>
								</div>
								<div class="col-sm-3 apply-col hidden-xs">
									<a href="<?php echo $p_application_page_url; ?>" class="apply-btn btn btn-skelleton btn-rounded medium btn-red-1">
										<?php echo $apply_text; ?>
									</a>
								</div>
							</div>
						</h4>
					</div>
					<div id="position-detail-<?php echo $count; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="postion-<?php echo $count; ?>">
						<div class="panel-body">
							<div class="job-content-title"><?php echo $job_description_title; ?></div>
							<div class="job-content">
								<?php echo $job_description; ?>
							</div>

							<div id="qualification-description-title" class="job-content-title"><?php echo $qualification_description_title; ?></div>
							<div class="job-content">
								<?php echo $qualification_description; ?>
							</div>

							<div class="button-panel text-center">
								<a href="<?php echo $p_application_page_url; ?>" class="apply-btn btn btn-skelleton btn-rounded medium btn-red-1">
									<?php echo $apply_text; ?>
								</a>
							</div>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
	<?php } ?>

	<section id="career-apply"
			 class="content-section animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container text-center">
			<p>
				<?php the_field('footer_text'); ?>
			</p>

			<div class="button-panel">
				<a href="<?php echo $application_page_url; ?>" class="apply-btn btn btn-skelleton btn-rounded medium btn-red-1 size-2">
					<?php echo $apply_now_text; ?>
				</a>
			</div>
		</div>
	</section>
</div>
<?php get_footer();