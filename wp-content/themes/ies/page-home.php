<?php
/**
 * The main template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;
$post_slug = $post->post_name;
$page_title = get_the_title();

$wwd_title = get_field('what_we_do_title');

// Get all available project types
$args = array(
	'posts_per_page'   => '-1',
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	// 'orderby'          => 'date',
	// 'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	// 'meta_key'         => '',
	// 'meta_value'       => '',
	'post_type'        => 'project_type',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$all_project_types_query = get_posts($args);
$all_project_types = array();

foreach ($all_project_types_query as $post) : setup_postdata($post);
	$type_id = $post->ID;
	$type_icon = get_field('icon');
	$type_title = get_field('project_type_title');
	$type_subtitle = get_field('project_type_subtitle');
	$type_description = get_field('project_type_description');

	$type_info = array(
		"id" => $type_id,
		"icon" => $type_icon,
		"title" => $type_title,
		"subtitle" => $type_subtitle,
		"description" => $type_description
	);

	$all_project_types[$type_id] = $type_info;
endforeach;
wp_reset_postdata();

// Get contact page id
$project_page = get_page_by_path( 'projects' );
$project_page_id = $project_page->ID;
$project_page_url = get_page_link($project_page_id);

$view_projects_text = get_field('what_we_do_link_text');

$isl_background_image = get_field('isl_background_image');
$isl_text = get_field('isl_text');
$isl_text_color = get_field('isl_text_color');
$isl_logo = get_field('isl_logo');
$isl_link_text = get_field('isl_link_text');
$isl_link_text_color = get_field('isl_link_text_color');
$isl_website_url = get_field('isl_website_url');
$slide_theme = get_field('slide_theme');

$first_mover_title = get_field('first_mover_title');
$first_movers = get_field('first_movers');

$about_page = get_page_by_path( 'about' );
$about_page_id = $about_page->ID;
$ies_number_title = get_field('ies_number_title', $about_page_id);
$ies_number_description = get_field('ies_number_description', $about_page_id);
$ies_numbers = get_field('ies_numbers', $about_page_id);

$customer_title = get_field('customer_title');
$testimonials = get_field('testimonials');

$people_title = get_field('people_title');
$people_button_text = get_field('people_button_text');
$people_image = get_field('people_background_image');
$people_button_url = get_field('people_button_url');

$staff_voices = get_field('staff_voices');

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);

$start_project_with_us_text = get_field("start_project_with_us_text", $contact_page_id);

$page_style = "opacity: 0;";
?>

<div id="home-page" style="<?php echo $page_style; ?>">
	<input type="hidden" id="slide-theme" value="<?php echo $slide_theme; ?>">

	<?php
	if ( wp_get_referer() ){
		$referer = wp_get_referer();
		$base_url = get_site_url();

		if(!strstr($referer, $base_url)){
	?>
	<section id="splashscreen">
		<img src="<?php echo get_template_directory_uri() . "/images/ies-logo.svg"; ?>" 	alt=""
			 class="blink animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
	</section>
	<?php
		}
	}else{ ?>
	<section id="splashscreen">
		<img src="<?php echo get_template_directory_uri() . "/images/ies-logo.svg"; ?>" 	alt=""
			 class="blink animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
	</section>
	<?php } ?>
	<section id="slide-panel"
			 class="<?php echo $slide_theme; ?> no-top-space">
		<?php
		$slide_title = get_field('slide_title');
		$slide_description = get_field('slide_description');

		if( have_rows('slide_button') ):
		    if( have_rows('slide_button') ) : the_row();
		        $display_button = get_sub_field('display_button');

		        if($display_button){
					$button_text = get_sub_field('button_text');
					$button_link = get_sub_field('button_link');
				}
		    endif;

		    reset_rows();
		endif;
		?>

		<div id="slide-content-container" class="container">
			<div class="slide-content">
				<div class="slide-content-inner">
					<div class="title"><?php echo $slide_title; ?></div>
					<div class="description"><?php echo $slide_description; ?></div>
					<?php if($display_button): ?>
					<div class="button-panel">
						<a id="slide-button" href="<?php echo $button_link; ?>" class="btn btn-red-1 btn-rounded extra-big"><?php echo $button_text; ?></a>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div id="explore-button-panel" class="text-center hide">
			<a id="explore-button" href="" class="blink">
				<div>Slide to explore</div>
				<div><i class="fa fa-angle-down"></i></div>
			</a>
		</div>

		<?php
		$drawing_image = get_field('drawing_image')['url'];
		$actual_image = get_field('actual_image')['url'];
		?>
		<figure class="cd-image-container">
			<div class="mask-menu"></div>

			<img src="<?php echo $actual_image; ?>" alt="">

			<div class="cd-resize-img">
				<img src="<?php echo $drawing_image; ?>" alt="">
			</div>

			<span class="cd-handle"></span>
		</figure>
	</section>

	<section id="what-we-do" class="no-top-space top-nav">
		<div class="container">
			<img src="<?php echo get_template_directory_uri() . '/images/clouds-2.png'; ?>" alt="" id="what-we-do-clouds" class="hidden-xs">
			<div class="section-title big"><?php echo $wwd_title; ?></div>
			<div id="project-type-list-panel">
				<ul id="project-type-list" class="timer row narrow">
					<?php
					$col_class = count($all_project_types) <= 4 ? "col-sm-6 col-md-4" : "";

					foreach($all_project_types as $project_type){
						$pt_id = $project_type['id'];
						$pt_icon = $project_type['icon'];
						$pt_title = $project_type['title'];
						$pt_subtitle = $project_type['subtitle'];

						$search_url = $pt_id == 0 ? $project_page_url : $project_page_url . '?nproject_type=' . $pt_id;
					?>
					<li class="project-type col-xs-6 col-sm-12">
						<a href="<?php echo $search_url; ?>">
							<div class="project-type-icon">
								<img src="<?php echo $pt_icon['url']; ?>" alt="">
							</div>
							<div class="project-type-title"><?php echo $pt_title; ?></div>
							<div class="project-type-link btn link-btn text-red-1 more-detail fixed"><?php echo $view_projects_text; ?></div>
						</a>
					</li>
					<?php } ?>
				</ul>
			</div>

			<img id="solar-cell-image" src="<?php echo get_template_directory_uri() . '/images/solar-cell.png'; ?>" alt="">
		</div>
	</section>

	<?php if($first_movers && count($first_movers) > 0){ ?>
	<section id="first-mover"
			 class="no-top-space animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container">
			<div class="section-title big"><?php echo $first_mover_title; ?></div>

			<div id="first-mover-container">
				<?php
				$time = 0.1;

				for($i = 0; $i < count($first_movers); $i++){
					$first_mover = $first_movers[$i];
					$first_mover_title = $first_mover['first_mover_title'];
					$first_mover_description = $first_mover['first_mover_description'];
					$first_mover_image = $first_mover['first_mover_image'];
					$first_mover_link_text = $first_mover['first_mover_link_text'];
					$first_mover_link = $first_mover['first_mover_url'];

					$components = parse_url($first_mover_link);
					$is_external = false;
					$base_url = $_SERVER['SERVER_NAME'];

					$is_external = !empty($components['host']) && strcasecmp($components['host'], $base_url);

					$col_class_1 = ($i % 2 == 0) ? "col-sm-push-6" : "";
					$col_class_2 = ($i % 2 == 0) ? "col-sm-pull-6" : "";

					$decor_class = "";

					switch($i){
						case 0:
							$decor_class = "butterfly";
							break;
						case 1:
							$decor_class = "cloud wind-turbine";
							break;
						case 2:
							// $decor_class = "wind-turbine";
							break;
						case 3:
							$decor_class = "sun";
							break;
					}

					$time += 0.15;
				?>
				<div class="first-mover-item item animate <?php echo $decor_class; ?>"
					 data-os-animation="fadeIn"
		 		 	 data-os-animation-delay="<?php echo $time; ?>s">
					<div class="row no-gap-desktop">
						<div class="first-mover-col col-sm-6 <?php echo $col_class_1; ?>">
							<div class="first-mover-content">
								<div class="first-mover-title"><?php echo $first_mover_title; ?></div>

								<div class="first-mover-short-description">
									<p>
										<?php echo $first_mover_description; ?>
									</p>

									<div class="button-panel">
										<a href="<?php echo $first_mover_link; ?>" <?php echo $is_external ? 'target="blank"' : ''; ?> class="btn first-mover-link-btn link-btn text-red-1 more-detail"><?php echo $first_mover_link_text; ?></a>
									</div>
								</div>
							</div>
						</div>
						<div class="first-mover-col first-mover-image-col col-sm-6 <?php echo $col_class_2; ?>">
							<div class="first-mover-image" style="background-image: url(<?php echo $first_mover_image; ?>);"></div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
	<?php } ?>

	<?php if($ies_numbers && count($ies_numbers) > 0){ ?>
	<section id="ies-in-number"
			 class="content-section animate no-top-space"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="section-title text-center"><?php echo $ies_number_title; ?></div>
			<div class="row">
				<?php
				$col_class = "col-sm-12";

				if(count($ies_numbers) == 2){
					$col_class = "col-sm-6";
				}else if(count($ies_numbers) == 3){
					$col_class = "col-sm-4";
				}else if(count($ies_numbers) == 4){
					$col_class = "col-sm-3";
				}

				for($i = 0; $i < count($ies_numbers); $i++){
					$number = $ies_numbers[$i]['number'];
					$title = $ies_numbers[$i]['title'];
				?>
				<div class="magic-number-col <?php echo $col_class; ?>">
					<div class="magic-number-box">
						<div class="magic-number" data-value="<?php echo $number; ?>">0</div>
						<div class="number-description"><?php echo $title; ?></div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
	<?php } ?>

	<?php if($testimonials && count($testimonials) > 0){ ?>
	<section id="customer-trust"
			 class="content-section no-top-space animate top-nav"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container">
			<div class="section-title text-center big"><?php the_field('customer_title'); ?></div>
			<div id="customer-trust-list" class="timer staff-voice-list row owl-carousel owl-theme">
				<?php
				$t_count = 0;
				$t_limit = 10;

				foreach ($testimonials as $testimonial):
					$t_count++;

					if($t_count > $t_limit){
						break;
					}

					$c_name = $testimonial['customer_name'];
					$c_position = $testimonial['customer_position'];
					$c_message = $testimonial['customer_message'];
				?>
				<div class="testimonial item">
					<div class="text"><?php echo $c_message; ?></div>
					<div class="customer-profile">
						<div class="name"><?php echo $c_name; ?></div>
						<div class="position"><?php echo $c_position; ?></div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<?php } ?>

	<section id="our-people"
			 class="section-cover-image no-top-space animate"
			 style="background-image: url(<?php echo $people_image; ?>);"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container text-center">
			<div class="mask white hide"></div>
			<div class="section-content">
				<div id="our-people-title" class="section-title big">
					<?php echo $people_title; ?>
				</div>
				<div class="button-panel">
					<a id="" href="<?php echo $people_button_url; ?>" class="btn btn-red-1 btn-rounded btn-skelleton extra-big"><?php echo $people_button_text; ?></a>
				</div>
			</div>
		</div>
	</section>

	<?php if($staff_voices && count($staff_voices) > 0){ ?>
	<section id="staff-voices"
			 class="no-top-space animate top-nav"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container">
			<div class="section-title text-center"><?php the_field('staff_voices_title'); ?></div>
			<div id="staff-voice-list" class="timer staff-voice-list row owl-carousel owl-theme">
				<?php
				global $post;

				$s_count = 0;
				$s_limit = 10;

				foreach ($staff_voices as $post) : setup_postdata($post);
					$s_count++;

					if($s_count > $s_limit){
						break;
					}

					if(!isset($post['staff_voice']) || !isset($post['staff_voice']->ID)){
						continue;
					}

					$p_id = $post['staff_voice']->ID;
					$firstname = get_field("staff_firstname", $p_id);
					$lastname = get_field("staff_lastname", $p_id);
					$name = $firstname . ' ' . $lastname;
					$position = get_field("role_title", $p_id);
					$voice = get_field("staff_voice", $p_id);
					$image = get_field("image", $p_id);
				?>
				<div class="staff-voice item">
					<div class="text">“<?php echo $voice; ?>”</div>
					<div class="staff-profile">
						<div class="profile-thumbnail" style="background-image: url(<?php echo $image; ?>);"></div>
						<div class="profile-content">
							<div class="name"><?php echo $name; ?></div>
							<div class="position"><?php echo $position; ?></div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
	<?php } ?>

	<section id="isl"
			 class="section-cover-image animate no-top-space"
			 style="background-image: url(<?php echo $isl_background_image; ?>);"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container text-center">
			<div class="mask white hide"></div>
			<div class="section-content">
				<div>
					<img class="isl-logo" src="<?php echo $isl_logo; ?>" alt="">
				</div>
				<div class="isl-text" style="color: <?php echo $isl_text_color; ?>"><?php echo $isl_text; ?></div>
				<div class="button-panel no-margin-bottom">
					<a target="_blank"
					   href="<?php echo $isl_website_url; ?>"
					   class="isl-link btn link-btn more-detail"
					   style="color: <?php echo $isl_link_text_color; ?>">
						<?php echo $isl_link_text; ?>
					</a>
				</div>
			</div>
		</div>
	</section>

	<section id="home-pre-footer"
			 class="no-top-space pre-footer text-center animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="pre-footer-content">
				<div class="title-1 text-primary">
					<?php echo $start_project_with_us_text; ?>

					<a href="<?php echo $contact_page_url; ?>" class="icon text-red-1">
						<img src="<?php echo get_template_directory_uri() . '/images/right-arrow.svg'; ?>" alt="">
					</a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer();