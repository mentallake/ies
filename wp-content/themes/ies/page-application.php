<?php
/**
 * The application template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;
$post_slug = $post->post_name;
$page_title = get_the_title();

$apply_job_text = get_field('apply_job_text');
$application_introduction_text = get_field('application_introduction_text');
$job_position_label_text = get_field('job_position_label_text');
$job_position_placeholder_text = get_field('job_position_placeholder_text');
$name_label_text = get_field('name_label_text');
$name_placeholder_text = get_field('name_placeholder_text');
$email_label_text = get_field('email_label_text');
$email_placeholder_text = get_field('email_placeholder_text');
$mobile_label_text = get_field('mobile_label_text');
$mobile_placeholder_text = get_field('mobile_placeholder_text');
$resume_label_text = get_field('resume_label_text');
$resume_description_text = get_field('resume_description_text');
$choose_file_text = get_field('choose_file_text');
$linkedin_label_text = get_field('linkedin_label_text');
$linkedin_placeholder_text = get_field('linkedin_placeholder_text');
$submit_button_text = get_field('submit_button_text');

$position_query = isset($_GET['position']) ? $_GET['position'] : false;
$location_query = isset($_GET['location']) ? $_GET['location'] : false;

$p_title = $position_query ? $position_query : $apply_job_text;
$p_subtitle = $position_query ? $apply_job_text : false;
$p_location = $location_query ? $location_query : false;

// Get careers page id
$careers_page = get_page_by_path( 'careers' );
$careers_page_id = $careers_page->ID;
$careers_page_url = get_page_link($careers_page_id);
?>

<div id="application-page" class="content-page">
	<input type="hidden" id="careers-page-url" value="<?php echo $careers_page_url; ?>">

	<section id="application">
		<div class="container">
			<form action="<?php echo get_template_directory_uri() . "/application_submitted.php"; ?>"
				  id="application-form"
				  class="ies-form"
				  method="post"
				  enctype="multipart/form-data">
				<input type="hidden" name="apply_position" value="<?php echo $position_query; ?>">
				<input type="hidden" name="apply_location" value="<?php echo $p_location; ?>">

				<div class="form-header text-center">
					<?php if($p_subtitle){ ?>
					<div class="subtitle"><?php echo $p_subtitle; ?></div>
					<?php } ?>
					<div class="title"><?php echo $p_title; ?></div>
					<?php if($p_location){ ?>
					<div class="location"><?php echo $p_location; ?></div>
					<?php } ?>
				</div>
				<div class="form-body">
					<div class="desc text-center">
						<?php echo $application_introduction_text; ?>
					</div>
					<?php if(!$p_subtitle){ ?>
					<div class="form-group">
			    		<label for="position-textbox"><?php echo $job_position_label_text; ?></label>
			    		<input type="text" class="form-control required" name="apply_position" id="position-textbox" placeholder="<?php echo $job_position_placeholder_text; ?>">
			  		</div>
					<?php } ?>
					<div class="form-group">
			    		<label for="name-textbox"><?php echo $name_label_text; ?></label>
			    		<input type="text" class="form-control required" name="apply_name" id="name-textbox" placeholder="<?php echo $name_placeholder_text; ?>">
			  		</div>
			  		<div class="form-group">
			    		<label for="email-textbox"><?php echo $email_label_text; ?></label>
			    		<input type="email" class="form-control required" name="apply_email" id="email-textbox" placeholder="<?php echo $email_placeholder_text; ?>">
			  		</div>
			  		<div class="form-group">
			    		<label for="mobile-textbox"><?php echo $mobile_label_text; ?></label>
			    		<input type="tel" class="form-control phone-keyboard-only" maxlength="10" name="apply_mobile" id="mobile-textbox" placeholder="<?php echo $mobile_placeholder_text; ?>">
			  		</div>
			  		<div class="form-group">
			    		<label for="cv-input-file"><?php echo $resume_label_text; ?></label>
			    		<div class="remark"><?php echo $resume_description_text; ?></div>
			    		<div class="upload-btn-wrapper">
				    		<a id="cv-upload-btn" class="input-file-wrapper upload-btn btn btn-rounded btn-red-1 btn-skelleton size-2">
				    			<?php echo $choose_file_text; ?>
				    			<input type="file" class="input-file required" id="cv-input-file" name="apply_cv" accept="application/pdf,application/doc,image/jpeg, image/png">
				    		</a>
							<div id="cv-filename" class="filename"></div>
			    		</div>
			  		</div>
			  		<div class="form-group">
			    		<label for="linkedin-textbox"><?php echo $linkedin_label_text; ?></label>
			    		<input type="text" class="form-control" name="apply_linkedin" id="linkedin-textbox" placeholder="<?php echo $linkedin_placeholder_text; ?>">
			  		</div>
			  		<div class="button-panel text-center">
			  			<input type="submit" class="btn btn-rounded btn-red-1 big" value="<?php echo $submit_button_text; ?>">
			  		</div>
				</div>
			</form>
		</div>
	</section>
</div>
<?php get_footer();