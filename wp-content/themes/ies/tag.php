<?php
/**
 * The tag template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;
$page_title = get_the_title();

$post_type_query = isset($_GET['ntype']) ? $_GET['ntype'] : false;

$args = array(
	'posts_per_page'   => '-1',
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	'orderby'          => 'meta_value',
	'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	'meta_key'         => 'date',
	// 'meta_value'       => '',
	'post_type'        => $post_type_query,
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	'tag' => $tag,
);

$tag_posts = get_posts($args);

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);
?>

<div id="tag-page" class="content-page">
	<?php if(count($tag_posts) > 0){ ?>
	<section id=""
			 class="content-section no-top-space animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="section-title extra-small">Result of <span class="tag-text"><?php echo $tag; ?></span></div>
			<?php if($post_type_query == 'news'){ ?>
			<ul class="news-list row grid">
				<?php
				global $post;

				$time = 0.3;

				foreach ($tag_posts as $post) : setup_postdata($post);
					$image = get_field('image');
					$title = get_field('news_title');
					$date = get_field('date');
					$detail_url = get_permalink();
					$time += 0.15;
				?>
				<li class="news-item col-sm-6 col-md-4 animate"
					data-os-animation="fadeIn"
	 		 		data-os-animation-delay="<?php echo $time; ?>s">
					<a href="<?php echo $detail_url; ?>">
						<div class="news-image" style="background-image: url(<?php echo $image; ?>);"></div>

						<div class="news-content">
							<div class="news-title">
								<?php echo $title; ?>
							</div>
							<div class="news-date"><?php echo get_fulldate_display($date); ?></div>
						</div>
					</a>
				</li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</ul>
			<div class="button-panel text-center hide">
	  			<a href="" class="btn btn-rounded btn-skelleton btn-red-1 big">
	  				Older news
	  			</a>
	  		</div>
	  		<?php } ?>
		</div>
	</section>
	<?php } ?>

	<section id="tag-pre-footer"
			 class="pre-footer text-center animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="pre-footer-content">
				<div class="title-1 text-primary">
					Start the project with us

					<a href="<?php echo $contact_page_url; ?>" class="icon text-red-1">
						<img src="<?php echo get_template_directory_uri() . '/images/right-arrow.svg'; ?>" alt="">
					</a>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();