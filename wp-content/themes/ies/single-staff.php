<?php
/**
 * Template for single staff page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
global $post;
$post_slug = $post->post_name;

$id = $post->id;
$firstname = get_field('staff_firstname');
$lastname = get_field('staff_lastname');
$name = $firstname . ' ' . $lastname;
$post = get_post($id);
$position = get_field('role_title');
$image = get_field('image');

// Get about us page id
$about_page = get_page_by_path( 'about' );
$about_page_id = $about_page->ID;
$about_page_url = get_page_link($about_page_id);
?>

<div id="single-staff-page" class="content-page">
	<section class="content-section big-top-space">
		<div class="container">
			<div class="row">
				<div id="profile-content-col" class="col-sm-8">
					<div class="profile-image visible-xs" style="background-image: url(<?php echo $image; ?>)"></div>
					<div class="profile-header">
						<div class="profile-name"><?php echo $name; ?></div>
						<div class="position"><?php echo $position; ?></div>
					</div>
					<div class="profile-body">
						<div id="about-panel" class="profile-content">
							<div class="profile-content-title"><?php echo the_field("staff_about_title", $about_page_id); ?></div>
							<?php echo the_field("description"); ?>
						</div>
						<div id="experience-panel" class="profile-content">
							<div class="profile-content-title"><?php echo the_field("staff_experience_title", $about_page_id); ?></div>
							<?php echo the_field("experience"); ?>
						</div>
					</div>
				</div>
				<div id="profile-image-col" class="col-sm-4">
					<div class="profile-image hidden-xs" style="background-image: url(<?php echo $image; ?>)"></div>
					<div id="education">
						<div class="profile-content-title"><?php echo the_field("staff_education_title", $about_page_id); ?></div>
						<?php echo htmlspecialchars(the_field("education")); ?>
					</div>
				</div>
			</div>

			<div class="post-footer">
				<a href="<?php echo $about_page_url; ?>" class="btn link-btn text-red-1 text-normal"><?php echo the_field("staff_back_to_about_page_link_text", $about_page_id); ?></a>

				<br><br><br>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>