<?php
/**
 * The contact template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;
$post_slug = $post->post_name;
$page_title = get_the_title();

$or_text = get_field('or_text');
$name_label_text = get_field('name_label_text');
$name_placeholder_text = get_field('name_placeholder_text');
$company_label_text = get_field('company_label_text');
$company_placeholder_text = get_field('company_placeholder_text');
$email_label_text = get_field('email_label_text');
$email_placeholder_text = get_field('email_placeholder_text');
$mobile_label_text = get_field('mobile_label_text');
$mobile_placeholder_text = get_field('mobile_placeholder_text');
$message_label_text = get_field('message_label_text');
$message_placeholder_text = get_field('message_placeholder_text');
$send_button_text = get_field('send_button_text');

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);
?>

<div id="contact-page" class="content-page">
	<section id="contact-intro"
			 class="content-section big-top-space animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title"><?php the_field('page_title'); ?></div>
				</div>
			</div>
			<div class="row no-gap">
				<div class="col-sm-5 animate"
					 data-os-animation="fadeIn"
	 		 		 data-os-animation-delay="0.35s">
					<div id="contact-description-1">
						<?php the_field('contact_info_description'); ?>
					</div>
					<div id="contact-info-panel">
						<?php if( have_rows('email_group') ): the_row();
							$label_title = get_sub_field('email_label_text');
							$value = get_sub_field('email');
						?>
						<div class="contact-info-row row">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="contact-icon-col">
									<img src="<?php echo get_template_directory_uri() . '/images/mail.svg'; ?>" alt="">
								</div>
								<div class="contact-content-col">
									<div class="contact-content-label"><?php echo $label_title; ?></div>
									<div class="contact-content-text"><?php echo $value; ?></div>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<?php if( have_rows('phone_group') ): the_row();
							$label_title = get_sub_field('phone_label_text');
							$value = get_sub_field('phone');
						?>
						<div class="contact-info-row row">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="contact-icon-col">
									<img src="<?php echo get_template_directory_uri() . '/images/phone.svg'; ?>" alt="">
								</div>
								<div class="contact-content-col">
									<div class="contact-content-label"><?php echo $label_title; ?></div>
									<div class="contact-content-text"><?php echo $value; ?></div>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<?php if( have_rows('address_group') ): the_row();
							$label_title = get_sub_field('address_label_text');
							$value = get_sub_field('address');
							$map_url = get_sub_field('ies_map_url');
							$view_map_text = get_sub_field('view_map_text');
						?>
						<div class="contact-info-row row">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="contact-icon-col">
									<img src="<?php echo get_template_directory_uri() . '/images/location.svg'; ?>" alt="">
								</div>
								<div class="contact-content-col">
									<div class="contact-content-label"><?php echo $label_title; ?></div>
									<div class="contact-content-text"><?php echo $value; ?></div>
									<?php if($map_url){ ?>
									<a target="_blank" href="<?php echo $map_url; ?>" id="view-on-map-btn" class="btn link-btn text-red-1 text-normal"><?php echo $view_map_text; ?></a>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-sm-2 text-center">
					<div id="or-text"><?php echo $or_text; ?></div>
				</div>
				<div class="col-sm-5 animate"
					 data-os-animation="fadeIn"
	 		 		 data-os-animation-delay="0.5s">
					<div id="contact-description-2">
						<?php the_field('contact_form_description'); ?>
					</div>

					<form id="contact-form" method="post" action="<?php echo get_template_directory_uri() . "/contact_submitted.php"; ?>">
						<input type="hidden" id="contact-page-url" value="<?php echo $contact_page_url; ?>">

				  		<div class="form-group">
				    		<label for="name-textbox"><?php echo $name_label_text; ?></label>
				    		<input type="text" name="contact_name" class="form-control required" id="name-textbox" placeholder="<?php echo $name_placeholder_text; ?>">
				  		</div>
				  		<div class="form-group">
				    		<label for="company-textbox"><?php echo $company_label_text; ?></label>
				    		<input type="text" name="contact_company" class="form-control required" id="company-textbox" placeholder="<?php echo $company_placeholder_text; ?>">
				  		</div>
				  		<div class="form-group">
				    		<label for="mobile-textbox"><?php echo $mobile_label_text; ?></label>
				    		<input type="tel" name="contact_mobile" class="phone-keyboard-only form-control" id="mobile-textbox" placeholder="<?php echo $mobile_placeholder_text; ?>">
				  		</div>
				  		<div class="form-group">
				    		<label for="email-textbox"><?php echo $email_label_text; ?></label>
				    		<input type="email" name="contact_email" class="form-control required" id="email-textbox" placeholder="<?php echo $email_placeholder_text; ?>">
				  		</div>
				  		<div class="form-group">
				    		<label for="message-textarea"><?php echo $message_label_text; ?></label>
				    		<textarea name="contact_message" class="form-control required" rows="5" placeholder="<?php echo $message_placeholder_text; ?>"></textarea>
				  		</div>
				  		<div class="button-panel text-center">
				  			<input type="submit" class="btn btn-rounded btn-red-1 big" value="<?php echo $send_button_text; ?>">
				  		</div>
				  	</form>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();