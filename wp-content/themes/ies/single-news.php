<?php
/**
 * Template for single news page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
global $post;
$post_slug = $post->post_name;

$current_post_id = $post->ID;
$image = get_field('image');
$title = get_field('news_title');
$date = get_field('date');
$news_content = get_field('news_content');
$tags = wp_get_post_tags($current_post_id, array( 'fields' => 'all' ));

// Get news page id
$news_page = get_page_by_path( 'news' );
$news_page_id = $news_page->ID;
$news_page_url = get_page_link($news_page_id);

$back_to_all_news_text = get_field('back_to_all_news_text', $news_page_id);

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);

$start_project_with_us_text = get_field("start_project_with_us_text", $contact_page_id);
?>

<div id="single-news-page" class="content-page">
	<section class="post-content-panel animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="post">
				<div class="post-heading">
					<div class="post-title">
						<?php echo $title; ?>
					</div>
					<div class="post-date">
						<?php echo get_fulldate_display($date); ?>
					</div>
				</div>
				<div class="post-content">
					<div class="post-body">
						<?php echo $news_content; ?>
					</div>
					<div class="post-footer">
						<?php if(count($tags) > 0){ ?>
						<ul class="post-tag">
							<li class="tag-label">Tags:</li>
							<?php
							for($i = 0; $i < count($tags); $i++){
								$tag_id = $tags[$i]->term_id;
								$tag_name = $tags[$i]->name;
								$tag_link = get_tag_link($tag_id) . "?ntype=news";
							?>
							<li><a href="<?php echo $tag_link; ?>"><?php echo $tag_name; ?></a></li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
				</div>

				<a href="<?php echo $news_page_url; ?>" class="btn link-btn text-red-1 text-normal"><?php echo $back_to_all_news_text; ?></a>

				<br><br><br>
			</div>
		</div>
	</section>

	<section id="news-pre-footer"
			 class="pre-footer text-center animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.35s">
		<div class="container">
			<div class="pre-footer-content">
				<div href="" class="title-1 text-primary">
					<?php echo $start_project_with_us_text; ?>

					<a href="<?php echo $contact_page_url; ?>" class="icon text-red-1">
						<img src="<?php echo get_template_directory_uri() . '/images/right-arrow.svg'; ?>" alt="">
					</a>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer();