<?php
/**
 * The about template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;

$post_slug = $post->post_name;
$page_title = get_field('page_title');
$introduction_image = get_field('introduction_image');
$introduction_title = get_field('introduction_title');
$introduction_description = get_field('introduction_description');
$factsheet = get_field('factsheet');
$factsheet_th = get_field('factsheet_th');
$factsheet_link_text = get_field('factsheet_link_text');
$people_title = get_field('people_title');
$people_description = get_field('people_description');
$ies_number_title = get_field('ies_number_title');
$ies_number_description = get_field('ies_number_description');
$ies_numbers = get_field('ies_numbers');

$langcode = qtranxf_getLanguage();

$args = array(
	'posts_per_page'   => '-1',
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	// 'orderby'          => 'date',
	// 'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	// 'meta_key'         => '',
	// 'meta_value'       => '',
	'post_type'        => 'staff',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$all_staff = get_posts($args);
$management = array();
$staff = array();

foreach ($all_staff as $post) : setup_postdata($post);
	$role_type = get_field('role_type');

	// Management
	if($role_type == 1){
		$management[] = $post;
	}else if($role_type == 2){ // Staff
		$staff[] = $post;
	}
endforeach;

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);

$start_project_with_us_text = get_field("start_project_with_us_text", $contact_page_id);
?>

<div id="about-page" class="content-page">
	<section id="about-intro"
			 class="content-section big-top-space animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<div class="page-title"><?php echo $page_title; ?></div>
					<div class="section-title"><?php echo $introduction_title; ?></div>
					<p>
						<?php echo $introduction_description; ?>
					</p>

					<?php
					$factsheet_url = false;

					if($langcode == 'en'){
						if($factsheet){
							$factsheet_url = $factsheet;
						}
					}else{
						if($factsheet_th){
							$factsheet_url = $factsheet_th;
						}
					}
					?>

					<?php if($factsheet_url){ ?>
					<div class="button-panel top-40 bottom-40">
						<a href="<?php echo $factsheet_url; ?>" target="_blank" class="btn link-btn text-red-1 text-normal"><img src="<?php echo get_template_directory_uri() . '/images/download.svg'; ?>" alt="">&nbsp;&nbsp;<?php echo $factsheet_link_text; ?></a>
					</div>
					<?php } ?>
				</div>
				<div class="col-sm-7">
					<div id="about-image" style="background-image: url(<?php echo $introduction_image; ?>);"></div>
				</div>
			</div>
		</div>
	</section>

	<section id="team"
			 class="content-section animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="section-title"><?php echo $people_title; ?></div>
			<p>
				<?php echo $people_description; ?>
			</p>

			<div id="management-staff">
				<ul class="staff-list management row">
					<?php
					$time = 0.2;
					$count = 0;

					foreach ($management as $post) : setup_postdata($post);
						$image = get_field('image');
						$firstname = get_field('staff_firstname');
						$lastname = get_field('staff_lastname');
						$position = get_field('role_title');
						$keyword = get_field('keyword');
						$name = $firstname . ' ' . $lastname;
						$detail_url = get_permalink();
						$kw_class = $keyword == "" ? "" : "keyword";

						$count++;

						if($count % 4 == 1){
							$time += 0.15;
						}
					?>
					<li class="col-sm-4 col-md-3 animate <?php echo $kw_class; ?>"
						data-os-animation="fadeInUp"
	 		 			data-os-animation-delay="<?php echo $time; ?>s">
						<a href="<?php echo $detail_url; ?>">
							<div class="profile-image" style="background-image: url(<?php echo $image; ?>)">
								<div class="staff-keyword">
									<div class="keyword"><?php echo $keyword; ?></div>
								</div>
							</div>
							<div class="name">
								<span class="name-text"><span class="firstname"><?php echo $firstname; ?></span> <span class="lastname"><?php echo $lastname; ?></span></span>
							</div>
							<div class="position"><?php echo $position; ?></div>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>

			<div id="staff">
				<ul class="staff-list row">
					<?php
					$time = 0.35;
					$count = 0;

					foreach ($staff as $post) : setup_postdata($post);
						$image = get_field('image');
						$firstname = get_field('staff_firstname');
						$lastname = get_field('staff_lastname');
						$name = $firstname . ' ' . $lastname;
						$position = get_field('role_title');
						$keyword = get_field('keyword');
						$name = $firstname . ' ' . $lastname;
						$detail_url = get_permalink();
						$kw_class = $keyword == "" ? "" : "keyword";

						$count++;

						if($count % 4 == 1){
							$time += 0.15;
						}
					?>
					<li class="col-xs-6 col-sm-4 col-md-3 animate <?php echo $kw_class; ?>"
						data-os-animation="fadeInUp"
	 		 			data-os-animation-delay="<?php echo $time; ?>s">
						<div>
							<div class="profile-image" style="background-image: url(<?php echo $image; ?>)">
								<div class="staff-keyword">
									<div class="keyword"><?php echo $keyword; ?></div>
								</div>
							</div>
							<div class="name">
								<span class="name-text"><?php echo $name; ?></span>
							</div>
							<div class="position"><?php echo $position; ?></div>
						</div>
					</li>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</ul>
			</div>
		</div>
	</section>

	<?php if(count($ies_numbers) > 0){ ?>
	<section id="ies-in-number"
			 class="content-section animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="section-title text-center"><?php echo $ies_number_title; ?></div>
			<div class="row">
				<?php
				$col_class = "col-sm-12";

				if(count($ies_numbers) == 2){
					$col_class = "col-sm-6";
				}else if(count($ies_numbers) == 3){
					$col_class = "col-sm-4";
				}else if(count($ies_numbers) == 4){
					$col_class = "col-sm-3";
				}

				while( have_rows('ies_numbers') ) : the_row();
					$number = get_sub_field('number');
					$title = get_sub_field('title');
				?>
				<div class="magic-number-col <?php echo $col_class; ?>">
					<div class="magic-number-box">
						<div class="magic-number" data-value="<?php echo $number; ?>">0</div>
						<div class="number-description"><?php echo $title; ?></div>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
	<?php } ?>

	<?php
	$about_images = get_field('about_images');

	if(count($about_images) > 0){
 	?>
	<section id="about-footer-image-panel"
			 class="content-section no-space hidden-xs animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container-fluid">
			<div class="row">
				<?php
				$col_class = "col-sm-12";

				if(count($about_images) == 2){
					$col_class = "col-sm-6";
				}

				foreach( $about_images as $image ):
				?>
				<div class="<?php echo $col_class; ?> footer-image-box" style="background-image: url(<?php echo $image['url']; ?>);">
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<?php } ?>

	<section id="about-pre-footer"
			 class="pre-footer text-center animate"
			 data-os-animation="fadeIn"
	 		 data-os-animation-delay="0.3s">
		<div class="container">
			<div class="pre-footer-content">
				<div class="title-1 text-primary">
					<?php echo $start_project_with_us_text; ?>

					<a href="<?php echo $contact_page_url; ?>" class="icon text-red-1">
						<img src="<?php echo get_template_directory_uri() . '/images/right-arrow.svg'; ?>" alt="">
					</a>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer();