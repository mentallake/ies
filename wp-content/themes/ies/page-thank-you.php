<?php
/**
 * The thank you template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;

$link_text = get_field("back_to_home_page_text");

$content_type = isset($_GET['ncontent_type']) ? $_GET['ncontent_type'] : false;

$title = "Thank you";
$text = "Thank you for your interest in us.<br>We will contact you within 1-2 business days.";

if($content_type){
	if($content_type == 'application'){
		$title = get_field("application_submission_title");
		$text = get_field("application_submission_description");
	}else if($content_type == 'contact'){
		$title = get_field("contact_submission_title");
		$text = get_field("contact_submission_description");
	}
}

$home_page = get_page_by_path( 'home' );
$home_page_id = $home_page->ID;
$home_page_url = get_page_link($home_page_id);
?>
<div id="thankyou-page" class="content-page">
	<div class="container text-center">
		<div class="thankyou-icon">
			<img src="<?php echo get_template_directory_uri() . '/images/success-icon@3x.png'; ?>" alt="">
		</div>
		<div class="thankyou-title"><?php echo $title; ?></div>
		<div class="thankyou-text"><?php echo $text; ?></div>
		<div class="button-panel">
			<a href="<?php echo $home_page_url; ?>" class="btn link-btn text-red-1 text-normal">
				<?php echo $link_text; ?>
			</a>
		</div>
	</div>
</div>

<?php get_footer();